///@description scr_create_athlete(name, level, max10bounce)
///@param name
///@param level
///@param max10bounce
var a = ds_map_create();
ds_map_add(a, "name", argument0);
ds_map_add(a, "level", argument1);
ds_map_add(a, "max10bounce", argument2);
routines = ds_list_create();
ds_map_add_list(athlete, "routines", routines);
return a;