///@description scr_draw_3slice_horizontal(sprite, image, size, x1, x2, y, colour, alpha)
///@param sprite
///@param image
///@param size
///@param x1
///@param x2
///@param y
///@param colour
///@param alpha
var sprite = argument0;
var image = argument1;
var size = argument2;
var slice_x1 = argument3;
var slice_x2 = argument4;
var slice_y = argument5;
var slice_col = argument6;
var slice_alpha = argument7;

draw_sprite_part_ext(sprite, image, 0, 0, size, size, slice_x1, slice_y, 1, 1, slice_col, slice_alpha);
draw_sprite_part_ext(sprite, image, size, 0, size, size, slice_x1+size, slice_y,
	(slice_x2 - slice_x1 - 2*size) / size, 1, slice_col, slice_alpha);
draw_sprite_part_ext(sprite, image, size*2, 0, size, size, slice_x2-size, slice_y, 1, 1, slice_col, slice_alpha);