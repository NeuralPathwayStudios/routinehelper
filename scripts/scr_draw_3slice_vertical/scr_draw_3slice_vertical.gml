///@description scr_draw_3slice_vertical(sprite, image, size, x, y1, y2, colour, alpha)
///@param sprite
///@param image
///@param size
///@param x
///@param y1
///@param y2
///@param colour
///@param alpha
var sprite = argument0;
var image = argument1;
var size = argument2;
var slice_x = argument3;
var slice_y1 = argument4;
var slice_y2 = argument5;
var slice_col = argument6;
var slice_alpha = argument7;

draw_sprite_part_ext(sprite, image, 0, 0, size, size, slice_x, slice_y1, 1, 1, slice_col, slice_alpha);
draw_sprite_part_ext(sprite, image, 0, size, size, size, slice_x, slice_y1+size,
	1, (slice_y2 - slice_y1 - 2*size) / size, slice_col, slice_alpha);
draw_sprite_part_ext(sprite, image, 0, size*2, size, size, slice_x, slice_y2-size, 1, 1, slice_col, slice_alpha);