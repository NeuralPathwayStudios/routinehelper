///@description scr_create_routine_voluntary()
var a = ds_map_create();
ds_map_add(a, "name", "Routine");
ds_map_add(a, "type", "Voluntary");
ds_map_add(a, "dd", 0.0);

var m = ds_list_create();
ds_map_add_list(a, "moves", m);

for (var i = 0; i <= 10; i++) {
	var s = ds_map_create();
	ds_list_add(m, s);
	ds_map_add(s, "name", "");
	ds_map_add(s, "fig", "");
	ds_map_add(s, "dd", 0.0);
	ds_map_add(s, "use_dd", true);
}

return a;