/// @description Load and Init Variables
// App state (basically the current screen or transition)
state		= STATE.TITLE;
substate	= SUBSTATE.INITIALIZE;

// Data structure containing everything (should be loaded from JSON eventually)
data = ds_map_create();

athletes = ds_list_create();
ds_map_add_list(data, "athletes", athletes);

athlete = ds_map_create();
ds_list_add(athletes, athlete);
ds_map_add(athlete, "name", "Gabriel Carvalho");
ds_map_add(athlete, "level", "Espoir");
ds_map_add(athlete, "max10bounce", 21.37);

routines = ds_list_create();
ds_map_add_list(athlete, "routines", routines);

routine = ds_map_create();
ds_list_add(routines, routine);
ds_map_add(routine, "name", "Dream Routine");
ds_map_add(routine, "type", "Voluntary");
ds_map_add(routine, "dd", 13.5);

moves = ds_list_create();
ds_map_add_list(routine, "moves", moves);

#region skills
skill1 = ds_map_create();
ds_list_add(moves, skill1);
ds_map_add(skill1, "name", "Triffis Pike");
ds_map_add(skill1, "fig", "12--1<");
ds_map_add(skill1, "dd", 2.0);
ds_map_add(skill1, "use_dd", true);

skill2 = ds_map_create();
ds_list_add(moves, skill2);
ds_map_add(skill2, "name", "Half Half Pike");
ds_map_add(skill2, "fig", "811<");
ds_map_add(skill2, "dd", 2.0);
ds_map_add(skill2, "use_dd", true);

skill3 = ds_map_create();
ds_list_add(moves, skill3);
ds_map_add(skill3, "name", "Rudi Out Pike");
ds_map_add(skill3, "fig", "8-3<");
ds_map_add(skill3, "dd", 2.0);
ds_map_add(skill3, "use_dd", true);

skill4 = ds_map_create();
ds_list_add(moves, skill4);
ds_map_add(skill4, "name", "Double Back Pike");
ds_map_add(skill4, "fig", "8--<");
ds_map_add(skill4, "dd", 2.0);
ds_map_add(skill4, "use_dd", true);

skill5 = ds_map_create();
ds_list_add(moves, skill5);
ds_map_add(skill5, "name", "Half Out Pike");
ds_map_add(skill5, "fig", "8-1<");
ds_map_add(skill5, "dd", 2.0);
ds_map_add(skill5, "use_dd", true);

skill6 = ds_map_create();
ds_list_add(moves, skill6);
ds_map_add(skill6, "name", "Half Half Tuck");
ds_map_add(skill6, "fig", "8-1o");
ds_map_add(skill6, "dd", 2.0);
ds_map_add(skill6, "use_dd", true);

skill7 = ds_map_create();
ds_list_add(moves, skill7);
ds_map_add(skill7, "name", "Rudi Out Tuck");
ds_map_add(skill7, "fig", "8-3o");
ds_map_add(skill7, "dd", 2.0);
ds_map_add(skill7, "use_dd", true);

skill8 = ds_map_create();
ds_list_add(moves, skill8);
ds_map_add(skill8, "name", "Double Back Tuck");
ds_map_add(skill8, "fig", "8--o");
ds_map_add(skill8, "dd", 2.0);
ds_map_add(skill8, "use_dd", true);

skill9 = ds_map_create();
ds_list_add(moves, skill9);
ds_map_add(skill9, "name", "Half Out Tuck");
ds_map_add(skill9, "fig", "8-1o");
ds_map_add(skill9, "dd", 2.0);
ds_map_add(skill9, "use_dd", true);

skill10 = ds_map_create();
ds_list_add(moves, skill10);
ds_map_add(skill10, "name", "Full Full Straight");
ds_map_add(skill10, "fig", "822/");
ds_map_add(skill10, "dd", 2.0);
ds_map_add(skill10, "use_dd", true);
#endregion

// Current app dimensions
disp_width	= display_get_width();
disp_height = display_get_height();
aspect_ratio = disp_height / disp_width;
view_width = 1080; // use default width of 1080
view_height = 1080 * aspect_ratio;

camera = camera_create();
view_set_camera(0,camera);
camera_set_view_size(camera,view_width,view_height);

camera_set_view_pos(camera,0,0);



// Buttons
b_plus		= instance_create_layer(view_width*0.15, view_height*0.8+32, "Instances", o_plus);
b_question	= instance_create_layer(view_width*0.75-100, view_height*0.8+64, "Instances", o_question);
b_sort		= instance_create_layer(0, 0, "Instances", o_sort);
b_edit		= instance_create_layer(0, 0, "Instances", o_edit);
b_back		= instance_create_layer(0, 0, "Instances", o_back);

// mouse
mouse_x_last			= mouse_x;
mouse_y_last			= mouse_y;

// Title variables
title_scroll_amount		= 0;
title_scrolling			= false;
title_started_scrolling = false;
title_scroll_speed		= 0;
title_scroll_bar_alpha	= 0;

// New Athlete variables
field_name = instance_create_layer(view_width*0.15, view_height*0.4 - 200, "Instances", o_text_field);
field_name.text = "";
field_name.width = view_width * 0.7;
field_name.height = 200;
field_name.active = false;

field_level = instance_create_layer(view_width*0.15, view_height*0.4 - 200 + 250, "Instances", o_text_field);
field_level.text = "";
field_level.width = view_width * 0.3;
field_level.height = 200;
field_level.active = false;

field_10b = instance_create_layer(view_width*0.55, view_height*0.4 - 200 + 250, "Instances", o_text_field);
field_10b.text = "";
field_10b.width = view_width * 0.3;
field_10b.height = 200;
field_10b.active = false;

// Athlete view variables
view_athlete = pointer_null; // ds_map
view_athlete_name_width = 0;
athlete_scroll_amount		= 0;
athlete_scrolling			= false;
athlete_started_scrolling = false;
athlete_scroll_speed		= 0;
athlete_scroll_bar_alpha	= 0;
