{
    "id": "6cf171ca-7e7a-4d8c-b34b-594b866944f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_app",
    "eventList": [
        {
            "id": "a329a1b4-c6e0-4944-87b5-4e137f4a6f6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cf171ca-7e7a-4d8c-b34b-594b866944f8"
        },
        {
            "id": "c77948d6-7ff2-4f32-922c-85f5770556c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6cf171ca-7e7a-4d8c-b34b-594b866944f8"
        },
        {
            "id": "c8a20dd4-3a54-4745-9e53-82db60135dfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "6cf171ca-7e7a-4d8c-b34b-594b866944f8"
        },
        {
            "id": "f0b34808-8c84-42c5-b04e-85952539eda3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6cf171ca-7e7a-4d8c-b34b-594b866944f8"
        },
        {
            "id": "22734d3b-c08a-4db4-86e4-5f75d17a9c60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cf171ca-7e7a-4d8c-b34b-594b866944f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}