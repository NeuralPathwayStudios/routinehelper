/// @description Draw the app
var col_dark	= COL_DARK;
var col_light	= COL_LIGHT;
var col_back	= COL_BACK;

switch state {
	
	case STATE.TITLE:
			
		// Draw list of names
		draw_set_colour(col_dark);
		draw_set_font(f_50);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		var athlete_list = data[?"athletes"];
		for (var i = 0; i < ds_list_size(athlete_list); i++) {
			var current_athlete = athlete_list[|i];
			draw_text(view_width*0.15, view_height*0.40 + 32 + i*TITLE_NAME_SPACING - title_scroll_amount, current_athlete[?"name"]);
		}
		
		// Draw scroll bar if name list is big enough
		var athlete_list_size = ds_list_size(athlete_list) * TITLE_NAME_SPACING;
		if athlete_list_size > view_height*0.4 {
			if (title_scrolling || abs(title_scroll_speed) > 0) title_scroll_bar_alpha = 1.5;
			title_scroll_bar_alpha -= 1/room_speed/0.2;
			
			if title_scroll_bar_alpha > 0 {
				var list_visible_ratio		= view_height*0.4 / athlete_list_size;
				var possible_scroll_amount	= athlete_list_size - view_height*0.4;
				var available_scroll_ratio	= 1 - list_visible_ratio;
				var current_scroll_ratio	= title_scroll_amount / possible_scroll_amount;
				scr_draw_3slice_vertical(s_vertical_line_3slice, 0, 7, view_width*0.85 - 8,
					view_height*0.4 * (1 + current_scroll_ratio*available_scroll_ratio) + 16,
					view_height*0.4 * (1 + current_scroll_ratio*available_scroll_ratio + list_visible_ratio) - 32,
					col_light, title_scroll_bar_alpha);
			}
		}
		
		// Draw two boxes to cover overflowing names
		draw_set_colour(col_back);
		draw_rectangle(0, -10, view_width, view_height*0.4, false);
		draw_rectangle(0, view_height*0.8, view_width, view_height+10, false);
		
		// Draw title
		draw_set_colour(col_dark);
		draw_set_font(f_100);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_text(view_width/2, view_height*0.18, "ROUTINE");
		draw_text(view_width/2, view_height*0.26, "HELPER");
		
		// Draw lines
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.15, view_width*0.85, view_height*0.4, col_dark, 1);
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.15, view_width*0.85, view_height*0.8, col_dark, 1);
	
	break;
	case STATE.NEW_ATHLETE:
	
		// Draw lines
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.15, view_width*0.85, view_height*0.4, col_dark, 1.0);
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.15, view_width*0.45, view_height*0.4+250, col_dark, 1.0);
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.55, view_width*0.85, view_height*0.4+250, col_dark, 1.0);
			
		// Text under lines
		draw_set_colour(col_dark);
		draw_set_font(f_30);
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
		draw_text(view_width*0.5,view_height*0.4+20,"name");
		draw_text(view_width*0.3,view_height*0.4+250+20,"level");
		draw_text(view_width*0.7,view_height*0.4+250+20,"10 bounce");
	
		// Draw "NEW ATHLETE", "CANCEL", and "SAVE"
		draw_set_colour(col_dark);
		draw_set_font(f_70);
		draw_set_valign(fa_middle);
		draw_text(view_width/2, view_height*0.18, "NEW ATHLETE");

		draw_set_valign(fa_bottom);
		draw_set_halign(fa_left);
		draw_text(view_width*0.15, view_height*0.9, "BACK");
		draw_set_halign(fa_right);
		draw_text(view_width*0.85, view_height*0.9, "SAVE");
		
		

	
	break;
	case STATE.VIEW_ATHLETE:
		
		var top_height = VIEW_ATHLETE_NAME_POS + VIEW_ATHLETE_NAME_HEIGHT + VIEW_ATHLETE_DETAILS_HEIGHT;
		
		// Draw the list of routines
		var routine_list = ds_map_find_value(view_athlete, "routines");
		var ly = top_height - athlete_scroll_amount + 48; // list y
		var lx = view_width * 0.1; // list x
		draw_set_colour(col_dark);
		draw_set_valign(fa_top);
		for (var i = 0; i < ds_list_size(routine_list); i++) {
			var current_routine = routine_list[|i];
			draw_set_font(f_50b);
			draw_set_halign(fa_left);
			draw_text(lx, ly, ds_map_find_value(current_routine, "name"));
			
			ly += 88;
			draw_set_font(f_40);
			draw_text(lx, ly, "Type: " + ds_map_find_value(current_routine, "type"));
			draw_set_halign(fa_right);
			draw_text(view_width*0.9 - 32, ly, "DD: " + string_format(ds_map_find_value(current_routine, "dd"), 0, 1));
			
			ly += 100;
			var skills = ds_map_find_value(current_routine, "moves");
			for (var j = 1; j <= 10; j++) {
				draw_set_halign(fa_left);
				var current_skill = skills[|j - 1];
				draw_text(lx + 132, ly, ds_map_find_value(current_skill, "name"));
				draw_set_font(f_40b);
				draw_set_halign(fa_right);
				draw_text(lx + 100, ly, string(j) + ".");
				draw_set_font(f_40);
				if ds_map_find_value(current_skill, "use_dd") {
					draw_text(view_width*0.9 - 32, ly, string_format(ds_map_find_value(current_skill, "dd"), 0, 1));
				}
				ly += 88;
			}
			
			ly += 100;
			
			// Break if the list gets too long
			if ly > view_height - VIEW_ATHLETE_BOTTOM_HEIGHT break;
		}
		
		// Draw scroll bar if routine list is big enough
		var routine_list_size = ds_list_size(routine_list) * 1168;
		if ds_list_size(routine_list) > 0 {
			if (athlete_scrolling || abs(athlete_scroll_speed) > 0) athlete_scroll_bar_alpha = 1.5;
			athlete_scroll_bar_alpha -= 1/room_speed/0.2;
			
			if athlete_scroll_bar_alpha > 0 {
				var list_height = (view_height-top_height-VIEW_ATHLETE_BOTTOM_HEIGHT+32)
				var list_visible_ratio		= list_height / routine_list_size;
				var possible_scroll_amount	= routine_list_size - list_height;
				var available_scroll_ratio	= 1 - list_visible_ratio;
				var current_scroll_ratio	= athlete_scroll_amount / possible_scroll_amount;
				scr_draw_3slice_vertical(s_vertical_line_3slice, 0, 7, view_width*0.9 - 8,
					top_height + list_height * (current_scroll_ratio*available_scroll_ratio) + 16,
					top_height + list_height * (current_scroll_ratio*available_scroll_ratio + list_visible_ratio) - 32,
					col_light, athlete_scroll_bar_alpha);
			}
		}
		
		// Draw some boxes to hide portions of the list
		draw_set_colour(col_back);
		draw_rectangle(0,0,view_width, top_height, false);
		draw_rectangle(0,view_height - VIEW_ATHLETE_BOTTOM_HEIGHT,view_width, view_height, false);
		
		// Draw the top gui
		var gui_pos = VIEW_ATHLETE_NAME_POS;
		
		// Athlete name
		draw_set_colour(col_dark);
		draw_set_font(f_70);
		draw_set_valign(fa_top);
		draw_set_halign(fa_center);
		draw_text(view_width/2, gui_pos, ds_map_find_value(view_athlete, "name"));
		
		// Name lines
		view_athlete_name_width = string_width(ds_map_find_value(view_athlete, "name"));
		if view_athlete_name_width < view_width*0.7 {
			scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
				view_width*0.1, view_width*0.5 - view_athlete_name_width/2 - 32, gui_pos + 42, col_dark, 1.0);
			scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
				view_width*0.5 + view_athlete_name_width/2 + 32, view_width*0.9, gui_pos + 42, col_dark, 1.0);
		}
		
		// Athlete details
		gui_pos += VIEW_ATHLETE_NAME_HEIGHT;
		draw_set_font(f_50);
		draw_set_halign(fa_left);
		draw_text(view_width*0.1, gui_pos, ds_map_find_value(view_athlete, "level"));
		draw_set_halign(fa_right);
		draw_text(view_width*0.9, gui_pos, ds_map_find_value(view_athlete, "max10bounce"));
		
		// Draw line
		gui_pos += VIEW_ATHLETE_DETAILS_HEIGHT;
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.1, view_width*0.9, gui_pos, col_dark, 1.0);
		// total gui height is 
		
		
		
		// Draw bottom gui
		gui_pos = view_height - VIEW_ATHLETE_BOTTOM_HEIGHT;
		scr_draw_3slice_horizontal(s_horizontal_line_3slice, 0, 7,
			view_width*0.1, view_width*0.9, gui_pos, col_dark, 1.0);
		
	break;
}

// Draw text fields
with o_text_field {
	if active {
		draw_set_font(font);
		draw_set_halign(halign);
		draw_set_valign(valign);
		draw_set_colour(colour);
		draw_text(x + width/2, y + height - 8, text);
		
		if cursor_on {
			var xpos = x + (width + string_width(text)) / 2 + 5;
			scr_draw_3slice_vertical(s_vertical_line_3slice, 0, 7, xpos,
				y + height - 120, y + height -  20, colour, 1);
		}
	}
}

// Draw buttons
with o_button {
	if active draw_sprite_ext(sprite_index, image_index, x, y, 1, 1, 0, col_dark, 1);
}

// Copyright info
draw_set_colour(col_dark);
draw_set_font(f_30);
draw_set_halign(fa_center);
draw_set_valign(fa_bottom);
draw_text(view_width/2, view_height - 8, "Copyright 2019  |  Gabriel Carvalho");

draw_set_colour(c_white);

// debug info
//draw_set_font(f_50);
//draw_set_halign(fa_left);
//draw_set_valign(fa_top);
//draw_text(100, 100, title_started_scrolling);