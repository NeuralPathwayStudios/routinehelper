/// @description Define macros
enum STATE {
	TITLE,
	NEW_ATHLETE,
	VIEW_ATHLETE,
	VIEW_ROUTINE
}

enum SUBSTATE {
	INITIALIZE,
	TRANSITION,
	NORMAL
}

#macro COL_DARK		make_colour_rgb( 10,  25,  25  )
#macro COL_LIGHT	make_colour_rgb( 40,  100, 100 )
#macro COL_BACK		make_colour_rgb( 55,  130, 130 )

global.text_field = noone;

// Some UI spacings
#macro TITLE_NAME_SPACING 100
#macro VIEW_ATHLETE_BOTTOM_HEIGHT 256
#macro BUTTON_HEIGHT 100
#macro BUTTON_WIDTH 100
#macro VIEW_ATHLETE_NAME_POS 64
#macro VIEW_ATHLETE_NAME_HEIGHT 120
#macro VIEW_ATHLETE_DETAILS_HEIGHT 100