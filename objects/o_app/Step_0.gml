/// @description Control the app
switch state {

	case STATE.TITLE:
	
		switch substate {
			case SUBSTATE.INITIALIZE:
				field_name.active = false;
				field_level.active = false;
				field_10b.active = false;
				b_question.active = true;
				b_plus.active = true;
				b_back.active = false;
				b_edit.active = false;
				b_sort.active = false;
				
				b_plus.x = view_width*0.25;
				b_plus.y = view_height*0.8+64;
				
				substate = SUBSTATE.NORMAL;
			break;
			case SUBSTATE.NORMAL:
			
			break;
		}
		
		var athlete_list = data[?"athletes"];
		var athlete_list_size = ds_list_size(athlete_list) * TITLE_NAME_SPACING;
	
		if title_scrolling = false {
			// Check if list is long enough to scroll
			if athlete_list_size > view_height*0.4 {
				// Continue scrolling
				title_scroll_amount -= title_scroll_speed;
				var pre_clamp = title_scroll_amount;
				title_scroll_amount = clamp(title_scroll_amount, 0, athlete_list_size-view_height*0.4+32);
				if pre_clamp != title_scroll_amount title_scroll_speed = 0;
				title_scroll_speed = approach(title_scroll_speed, 1, 0);
				
				// Check for a mouse input to start scrolling
				if mouse_check_button_pressed(mb_left) {
					if (mouse_x > view_width*0.15 && mouse_x < view_width*0.85)
					&& (mouse_y > view_height*0.4 && mouse_y < view_height*0.8) {
						title_scrolling = true;
						title_started_scrolling = false;
					}
				}
			} else {
				title_scroll_amount = 0;
				title_scroll_speed = 0;
			}
		} else {
			// Check for release of mouse button
			if mouse_check_button_released(mb_left) {
				title_scrolling = false	
			} else {
				// Scroll the athlete list
				title_scroll_speed = mouse_y - mouse_y_last;
				if title_scroll_speed > 0 title_started_scrolling = true;
				title_scroll_amount -= title_scroll_speed;
				title_scroll_amount = clamp(title_scroll_amount, 0, athlete_list_size-view_height*0.4+32);
			}
		}
		
		
		// Check if a name has been pressed
		if !title_started_scrolling { // Hasn't scrolled up or down
			if mouse_check_button_released(mb_left) {
				if (mouse_x > view_width*0.15 && mouse_x < view_width*0.85)
				&& (mouse_y > view_height*0.4 && mouse_y < view_height*0.8) {
					// Pressed in the right area, check if y range aligns with a name
					var check_pos = view_height*0.4 - title_scroll_amount;
					for (var i = 0; i < ds_list_size(athlete_list); i++) {
						if mouse_y >= check_pos && mouse_y < check_pos + TITLE_NAME_SPACING {
							// Goto current athlete
							view_athlete = athlete_list[|i];
							state = STATE.VIEW_ATHLETE;
							substate = SUBSTATE.INITIALIZE;
						}
						check_pos += TITLE_NAME_SPACING;
						if check_pos > view_height*0.8 break;
					}
				}
			}
		}
	
	break;
	case STATE.NEW_ATHLETE:
		switch substate {
			case SUBSTATE.INITIALIZE:
				field_name.active = true;
				field_name.text = "";
				field_level.active = true;
				field_level.text = "";
				field_10b.active = true;
				field_10b.text = "";
				b_question.active = false;
				b_plus.active = false;
				b_back.active = false;
				b_edit.active = false;
				b_sort.active = false;
				substate = SUBSTATE.NORMAL;
			break;
			case SUBSTATE.NORMAL:
				// Check if buttons have been clicked
				if mouse_check_button_pressed(mb_left) {
					if mouse_y > view_height*0.9-100 && mouse_y < view_height*0.9+16 {
						if mouse_x > view_width*0.15 && mouse_x < view_width*0.4 {
							// Back button pressed
							state = STATE.TITLE;
							substate = SUBSTATE.INITIALIZE;
						} else if mouse_x > view_width*0.6 && mouse_x < view_width*0.85 {
							// Save button pressed
							// check if name is unique
							var athlete_list = ds_map_find_value(data, "athletes");
							var unique_name = true;
							for (var i = 0; i < ds_list_size(athlete_list); i++) {
								var current_athlete = athlete_list[|i];
								if ds_map_find_value(current_athlete, "name") == field_name.text {
									unique_name = false;
									break;
								}
							}
							
							if !unique_name {
								// Popup an error message or sth
							} else {
								// Create a new athlete and add them to the list
								var new_athlete = scr_create_athlete(field_name.text, field_level.text, field_10b.text);
								ds_list_add(athlete_list, new_athlete);
								
								// Goto next screen
								state = STATE.TITLE;
								substate = SUBSTATE.INITIALIZE;
							}
						}
					}
				}
				
			break;
		}
	break;
	case STATE.VIEW_ATHLETE:
		switch substate {
			case SUBSTATE.INITIALIZE:
				field_name.active	= false;
				field_level.active	= false;
				field_10b.active	= false;
				b_question.active	= false;
				b_back.active = true;
				b_plus.active = true;
				b_edit.active = true;
				b_sort.active = true;
				b_back.y = view_height - VIEW_ATHLETE_BOTTOM_HEIGHT/2 - BUTTON_HEIGHT/2 - 32;
				b_plus.y = view_height - VIEW_ATHLETE_BOTTOM_HEIGHT/2 - BUTTON_HEIGHT/2 - 32;
				b_edit.y = view_height - VIEW_ATHLETE_BOTTOM_HEIGHT/2 - BUTTON_HEIGHT/2 - 32;
				b_sort.y = view_height - VIEW_ATHLETE_BOTTOM_HEIGHT/2 - BUTTON_HEIGHT/2 - 32;
				b_back.x = view_width * (0.2 + 0.2 * 0) - BUTTON_WIDTH/2;
				b_plus.x = view_width * (0.2 + 0.2 * 1) - BUTTON_WIDTH/2;
				b_edit.x = view_width * (0.2 + 0.2 * 2) - BUTTON_WIDTH/2;
				b_sort.x = view_width * (0.2 + 0.2 * 3) - BUTTON_WIDTH/2;
				
				substate = SUBSTATE.NORMAL;
			break;
			case SUBSTATE.NORMAL:
			
				var routine_list = ds_map_find_value(view_athlete, "routines");
				var routine_list_size = ds_list_size(routine_list) * 1168;
				var top_height = VIEW_ATHLETE_NAME_POS + VIEW_ATHLETE_NAME_HEIGHT + VIEW_ATHLETE_DETAILS_HEIGHT;
				
				if athlete_scrolling = false {
					// Check if list is long enough to scroll
					if ds_list_size(routine_list) > 1 {
						// Continue scrolling
						athlete_scroll_amount -= athlete_scroll_speed;
						var pre_clamp = athlete_scroll_amount;
						athlete_scroll_amount = clamp(athlete_scroll_amount, 0, routine_list_size - (view_height-top_height-VIEW_ATHLETE_BOTTOM_HEIGHT+32));
						if pre_clamp != athlete_scroll_amount athlete_scroll_speed = 0;
						athlete_scroll_speed = approach(athlete_scroll_speed, 1, 0);
				
						// Check for a mouse input to start scrolling
						if mouse_check_button_pressed(mb_left) {
							if (mouse_x > view_width*0.1 && mouse_x < view_width*0.9)
							&& (mouse_y > top_height && mouse_y < view_height - VIEW_ATHLETE_BOTTOM_HEIGHT) {
								athlete_scrolling = true;
								athlete_started_scrolling = false;
							}
						}
					} else {
						athlete_scroll_amount = 0;
						athlete_scroll_speed = 0;
					}
				} else {
					// Check for release of mouse button
					if mouse_check_button_released(mb_left) {
						athlete_scrolling = false	
					} else {
						// Scroll the athlete list
						athlete_scroll_speed = mouse_y - mouse_y_last;
						if athlete_scroll_speed > 0 athlete_started_scrolling = true;
						athlete_scroll_amount -= athlete_scroll_speed;
						athlete_scroll_amount = clamp(athlete_scroll_amount, 0, routine_list_size - (view_height-top_height-VIEW_ATHLETE_BOTTOM_HEIGHT+32));
					}
				}
				
			break;
		}
	break;
}

// Useful mouse positions
mouse_x_last = mouse_x;
mouse_y_last = mouse_y;

// Hide keyboard
text_field_active = false;
with o_text_field if writing other.text_field_active = true;
if !keyboard_virtual_status() && text_field_active {
	keyboard_virtual_show(kbv_type_default, kbv_returnkey_default, kbv_autocapitalize_words, true);
} else if keyboard_virtual_status() && !text_field_active {
	keyboard_virtual_hide();
}
