{
    "id": "16853a5a-61e9-427f-84b4-e964c8f12067",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_question1",
    "eventList": [
        {
            "id": "53ed8434-c147-4914-8113-942416daf285",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16853a5a-61e9-427f-84b4-e964c8f12067"
        },
        {
            "id": "5eb1a895-1920-46e5-8d87-fe6922697e24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "16853a5a-61e9-427f-84b4-e964c8f12067"
        },
        {
            "id": "6e731903-7700-4b9b-9e16-a3bbba2fe11e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "16853a5a-61e9-427f-84b4-e964c8f12067"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ce080f79-ebd1-49e6-8a2d-217c435eb641",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a9690a1-f5d1-48c2-a240-3c564fd05c53",
    "visible": true
}