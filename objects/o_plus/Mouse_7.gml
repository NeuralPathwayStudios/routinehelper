if active {
	with o_app {
		switch state {
			case STATE.TITLE:
				state = STATE.NEW_ATHLETE;
				substate = SUBSTATE.INITIALIZE;
			break;
			case STATE.VIEW_ATHLETE:
				// New routine
				var new_routine = scr_create_routine_voluntary();
				var routine_list = ds_map_find_value(view_athlete, "routines");
				ds_list_add(routine_list, new_routine);
			break;
		}
	}
}