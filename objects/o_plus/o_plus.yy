{
    "id": "cb88d47d-5f96-4113-82df-fcbd28d6a047",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_plus",
    "eventList": [
        {
            "id": "c6d9feda-cb97-4148-908a-3293136f5f61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb88d47d-5f96-4113-82df-fcbd28d6a047"
        },
        {
            "id": "3b40de37-5a20-4388-94a9-d949689c92d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cb88d47d-5f96-4113-82df-fcbd28d6a047"
        },
        {
            "id": "159eec7c-69cf-44a9-ad20-0fa03595e5c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cb88d47d-5f96-4113-82df-fcbd28d6a047"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ce080f79-ebd1-49e6-8a2d-217c435eb641",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff4a7525-8104-47c6-a4a5-5d59e8f54576",
    "visible": true
}