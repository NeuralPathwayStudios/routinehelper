{
    "id": "3a055c7d-6d41-4cd7-ac8e-901c1baa3c5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_back1",
    "eventList": [
        {
            "id": "3b48cdff-1204-405e-b884-c7bf716ab16a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a055c7d-6d41-4cd7-ac8e-901c1baa3c5e"
        },
        {
            "id": "f1bd648d-3e69-4d78-ab9f-8fc3c8ad118d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "3a055c7d-6d41-4cd7-ac8e-901c1baa3c5e"
        },
        {
            "id": "32cab972-6302-484c-8994-dd354f37a2b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3a055c7d-6d41-4cd7-ac8e-901c1baa3c5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ce080f79-ebd1-49e6-8a2d-217c435eb641",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a9690a1-f5d1-48c2-a240-3c564fd05c53",
    "visible": true
}