{
    "id": "d876e4f5-51af-40f9-b71f-b420d2d3febe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_question",
    "eventList": [
        {
            "id": "b7c2a922-a907-4829-b9d9-715632da8628",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d876e4f5-51af-40f9-b71f-b420d2d3febe"
        },
        {
            "id": "c396fa2f-bc25-4382-9ed3-d2191d17bc6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "d876e4f5-51af-40f9-b71f-b420d2d3febe"
        },
        {
            "id": "fc45462f-6519-45f8-8ced-8cdcb9643f12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d876e4f5-51af-40f9-b71f-b420d2d3febe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ce080f79-ebd1-49e6-8a2d-217c435eb641",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a9690a1-f5d1-48c2-a240-3c564fd05c53",
    "visible": true
}