/// @description Write text
if active {
	
	if mouse_check_button_released(mb_left) {
		if mouse_x > x && mouse_x < x + width {
			if mouse_y > y && mouse_y < y + height {
				// Clicked
				writing = true;
				global.text_field = self;
				keyboard_string = text;
				cursor_on = true;
				cursor_timer = 0;
			} else {
				writing = false;
				cursor_on = false;
			}
		} else {
			writing = false;
			cursor_on = false;
		}
	}
	
	if writing {
		if global.text_field == self {
			text = keyboard_string;
			cursor_timer++;
			if text != text_last {
				cursor_timer = 0;
				cursor_on = true;
			} else if cursor_timer == cursor_time_max {
				cursor_timer = 0;
				cursor_on = !cursor_on;
			}
		} else {
			writing = false;
			cursor_on = false;
		}
	}

	text_last = text;
}