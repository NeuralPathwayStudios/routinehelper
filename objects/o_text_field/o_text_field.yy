{
    "id": "552f6be8-44a6-4b82-8a9e-da47c0652507",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_text_field",
    "eventList": [
        {
            "id": "48fa5f9c-0862-4e98-b71c-b69148e0bdce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "552f6be8-44a6-4b82-8a9e-da47c0652507"
        },
        {
            "id": "254fd91f-193b-4197-8c92-465f2b54da01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "552f6be8-44a6-4b82-8a9e-da47c0652507"
        },
        {
            "id": "9aed89d9-f329-428b-bb99-f0ea8e169e04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "552f6be8-44a6-4b82-8a9e-da47c0652507"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}