/// @description Variables
text = "";
active = true;
writing = false;
width = 100;
height = 100;
font = f_70;
halign = fa_center;
valign = fa_bottom;
colour = COL_DARK;

text_last = "";

cursor_timer = 0;
cursor_time_max = room_speed*0.5;
cursor_on = false;