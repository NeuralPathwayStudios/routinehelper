{
    "id": "dc4d2e5d-4fe0-4abc-8c6e-2464520d0f4b",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_30",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_30\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "22480c17-ca0f-4e2e-ab49-8a5148256bcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "47c354c6-a7a5-4f02-b188-d5a3f43acbb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 439,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a672816d-62dd-4560-b931-82faf9076864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 428,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "14930ebc-3fc0-4b62-99cd-db370522ab3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 400,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cf540f60-8486-4197-b37d-463d4d66c1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 376,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1a8c53aa-2764-4697-bc7a-7240c8ed2889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 1,
                "shift": 34,
                "w": 33,
                "x": 341,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "acd9a281-5792-4e6b-ab04-e04d848c1adf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 312,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "01602a3d-dcf2-44b8-a3a0-cb3b8030e9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 306,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "71bfb2b0-dde7-4936-b449-85b1fd7402c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 296,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ab1ad80b-ea5f-47fd-8cde-16021da07396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 285,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "95d4c37b-c18d-4407-a84c-da98e2e72913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 446,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d71a5658-ba52-45ac-a57a-df685cd8dbcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 263,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "91e188cb-1a4f-447e-a0e0-d0e1ead57b54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 232,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "119e7da5-fcae-43a7-9b65-269fba31efee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 217,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "14e30d0d-605b-4d58-b3c5-a814e6f503ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 210,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "886b848b-e71b-4db1-9747-f0aaf5c2aacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 191,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "76a84ae3-c111-4c4d-bf75-e8b275c39ac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 170,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0039e19a-6976-442c-b2a2-e6948f4ea8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 157,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0c3b3add-955a-465c-99df-26303c83b873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 135,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1b645a2b-8029-40c1-831c-67aa962ac8b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 113,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d38929f8-235f-4c40-a9f3-94ab2b32d742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 86,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1275b17e-faf5-4896-abb2-63bc827159aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 240,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c7ef0da2-613a-43af-9aee-58e33311eb0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 461,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e9f2ff87-5f15-43c3-9647-036457afdf55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 483,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1077d87e-c9df-4b85-b39e-936f45bca615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f838a4d0-4061-4ce7-bb28-034479e373af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d152e053-6d51-4aa7-ad6b-02802b30d43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 487,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1fe499f6-2dd1-465f-ba48-f667499cefcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 478,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f4830017-0945-48ac-b1c8-6859dff3be27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 456,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6774e3b4-29fb-4a2a-b901-694339caf5ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 436,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "701f620b-a939-407f-aaea-07229cd07105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 414,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e02aefa4-2a67-4c78-bad1-7d4df87babb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 392,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "14d75b7b-5298-4636-877e-8b5965494a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 348,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3852556c-51b4-4d7b-ad6a-09b87506d689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 318,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4a1104ad-a667-40af-a6f6-2d27146c01cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 4,
                "shift": 28,
                "w": 22,
                "x": 294,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cfc58872-cd04-4315-bf52-0170424f1c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 265,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "aadc7734-a50a-4cf6-a101-acc3feaf948a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 4,
                "shift": 31,
                "w": 25,
                "x": 238,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9af3603e-ee31-4969-8c09-0d455371479b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 4,
                "shift": 28,
                "w": 22,
                "x": 214,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "57a27c7f-205a-47ef-a35b-ce1739cbef05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 189,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2a8c1b0a-4901-4870-9190-6407edbbbc16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 160,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "380efd7a-f6ea-4e52-a314-f7ac02a51a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 4,
                "shift": 34,
                "w": 26,
                "x": 132,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2f888011-a3d2-4ba6-af25-798d51ee16b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 126,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "89bc1bb7-4316-485d-8dd6-63f6aeacd3ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 20,
                "x": 104,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6c07b250-303d-444c-84a1-d62a3241b5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 4,
                "shift": 26,
                "w": 21,
                "x": 81,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7382f2d9-8bcb-4531-a3a7-9adbda118a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 4,
                "shift": 25,
                "w": 21,
                "x": 58,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "27002e7d-559d-4464-866c-41db78458860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 24,
                "y": 146
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7e9d8de8-84a1-4dd5-8108-3a6e2450cb55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 4,
                "shift": 33,
                "w": 25,
                "x": 59,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1e494e9a-6f35-4816-a7dc-46421b9e9d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 24,
                "y": 98
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "31a749ce-f060-49f0-81a8-78f8a7bcc06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 4,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "70532001-70a6-4b85-99cf-6a0c1b92e686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e3f4305e-859f-4814-9022-3b10fc2e8ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 4,
                "shift": 25,
                "w": 21,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0cf10855-c651-411b-8528-43ad2d339021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1c2141f2-3d07-4bb4-b428-ea3dafcd5461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 421,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c7a79591-5abf-4df3-a320-baff4d8971fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 4,
                "shift": 33,
                "w": 25,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7918cb33-da44-4b73-a193-b488c96682e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 363,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fc01b12d-eb6f-4ed7-9f43-94a06cef4a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 37,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "87999956-9bd2-473e-adb0-650ca6428e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4dfd14ad-0119-4768-9535-95aaf4cfbc47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 268,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "986adf5a-ebad-4601-a154-f9b9d95d91e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "40161bcc-a32a-4563-b3cf-da6efbcdd47f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 496,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bb04fdcf-f620-4313-af8a-b3f0d58eadad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d6e1e4e9-0fba-4552-9d1a-7b90eab052e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "118a59cb-62c7-4c0d-853d-67af9515e55a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b23d7fc2-34ed-490e-9550-cd5db893d328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": -2,
                "shift": 19,
                "w": 23,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "16db1509-945b-463c-ad94-fac4e1d49540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0eaa821f-0422-4f4e-bd7a-239869d3cf23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "605800d2-c316-48f3-af65-c6253dd817be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "384df64e-bcca-452c-8d8b-63f3b7651375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f4afaeca-0839-48a0-88e9-c4d596cb74f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7de18c29-b226-45a5-97aa-8db8495c6633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eba27629-ceeb-4437-b9c5-dd7148dcafd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6e6107cc-e0c6-4f16-a49c-a0bc8d9bddf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6374b5cf-6dd9-4042-ad16-ad4788489a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 252,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8ac60ffc-ceee-4bc5-ad8a-6034e3125412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 62,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d8e66932-d21e-458b-b046-31c047a0cbf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -4,
                "shift": 11,
                "w": 12,
                "x": 472,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b1bc7bce-ad1c-4244-b60f-6d15c756935a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 452,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b075b5c3-092b-4b64-8be4-893d68d00a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 441,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "43853809-182d-48ba-8231-f03c729f7106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 407,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d0c1f1fe-dc65-4095-bd02-9001ac166af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 384,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5a15f533-0057-43ba-a590-e1e99df67755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 359,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ace75742-99b1-47c4-b4a5-590e8fa06b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 334,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e450ff6d-e663-415c-a9a3-64f79da3da9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 309,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3ebc0a4c-928f-4a4f-b582-b479d00a2a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 292,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "30ce7312-8151-4072-a44f-3703a4ce7db9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 486,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "456633f2-bfd2-4aec-b7b4-0076ab388999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 275,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "505b8cb7-3197-4f23-9231-f443c21737d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 229,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "396ee770-c1cd-45a5-98d2-0e2035339a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 205,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9d508fbe-d9f5-444d-9911-4f5a9868655f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 173,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "835df3fd-4df9-43be-8c73-6de1178e3805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a5f87464-863a-4653-9995-4c55e14f5cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 126,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "101c82a4-42b2-4afa-b8ae-96751e1e3eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 105,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "81031b21-90cd-48d1-aae2-a28f8f512970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9ec873e2-31a5-4cb9-a974-0e07a9a700ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c75130fe-59ce-4360-974d-c30863739a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 69,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bb735ae7-14de-4381-b953-b2b4c5dfab51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 24,
                "y": 194
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1dca8f0c-2160-4812-9dde-dcdf6a930837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 46,
                "offset": 8,
                "shift": 39,
                "w": 23,
                "x": 43,
                "y": 194
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}