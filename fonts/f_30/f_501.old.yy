{
    "id": "b7523361-18d8-4879-8d86-fcc45bdc5637",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_50",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_50\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9a85ba0f-eaf9-40bf-bf39-40aa32cacbc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2e14e0c7-961b-4c22-ae9d-c722bf1dee25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 74,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 275,
                "y": 154
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "01b6316b-b984-445f-ac69-d88861f24650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 259,
                "y": 154
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "83410670-2bfd-4ab9-add3-1d9063b97ac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 214,
                "y": 154
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6079ffde-527e-42fe-a30d-71f0a7fda89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 177,
                "y": 154
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "57d068a7-ebd7-4c76-a8a3-f44eef2891da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 74,
                "offset": 2,
                "shift": 57,
                "w": 54,
                "x": 121,
                "y": 154
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "891928d9-2bae-4705-879c-1c13d17b47d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 76,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7b0a54a2-65c7-4736-90c7-ed20e51716f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 74,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 68,
                "y": 154
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "99779efd-aabf-491b-b1ef-e9338e05ef77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 74,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 52,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4c44f569-2de1-42a2-b158-cacf68e48418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 74,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 36,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "98db47e1-e8d2-4c58-b476-0dab42915909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 286,
                "y": 154
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7feea6df-87ee-47b2-b4d5-76c7d4fc4f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "78483670-76c5-47d0-b0e5-2ca8b2a82470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 74,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 964,
                "y": 78
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fff53612-acd5-47d7-86ff-408b601e2c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 74,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 940,
                "y": 78
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "74561c72-e86f-4480-bd93-e72ef12ec668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 74,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 929,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "995a7777-638b-4ca4-882f-b1ce97661711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 900,
                "y": 78
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "79c3c7fa-b584-499c-a9a4-7693187ec7f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 866,
                "y": 78
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "946c2e04-b31d-415c-96b8-2d98f8bb383b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 74,
                "offset": 2,
                "shift": 25,
                "w": 17,
                "x": 847,
                "y": 78
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ef7c1198-c19c-414b-9aae-951ea89f5f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 813,
                "y": 78
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c79431b4-d511-4e04-85b1-5a18810c10c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 778,
                "y": 78
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "28e6d364-c8f9-4135-9935-d9a1479ac2a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 41,
                "x": 735,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1073ddd2-0f7d-4f56-8939-52eae5876ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 74,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 975,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ff3f1969-a2b2-43a2-8038-d81dd4893c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 308,
                "y": 154
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9697780f-8237-43de-b19d-fae68b37f8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 342,
                "y": 154
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ee3119c0-f71b-4d4d-a092-083b480db983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 377,
                "y": 154
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6541b945-f8b9-4896-8d63-395fee65e6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 158,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3f336a4f-4d59-4181-a73a-831180d75030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 148,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f6e47921-d572-4da5-b51c-39ce73845d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 136,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "36869da0-3802-476e-974f-6b7e253237df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 102,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5dcd9025-8040-4841-8706-16a4300f2e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 74,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 70,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f294ae46-2e51-4ad9-afe5-94397c90401d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 36,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1122d7a2-17d2-40af-af3a-50a0ee441410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "431dddb1-1328-4836-8b2e-c00491c83f04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 74,
                "offset": 2,
                "shift": 73,
                "w": 69,
                "x": 931,
                "y": 154
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8f18e2ed-5e50-451e-baad-4beb52703ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 74,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 882,
                "y": 154
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "72150d6b-4491-423f-bddb-b8c33e27bce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 37,
                "x": 843,
                "y": 154
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b820c68f-9ad0-42b2-93b9-1de971631eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 797,
                "y": 154
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2fc9d869-42eb-4af8-9142-f0f57d43e253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 74,
                "offset": 6,
                "shift": 51,
                "w": 42,
                "x": 753,
                "y": 154
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6e6567e6-b415-4bbc-af48-f2bc04e32f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 38,
                "x": 713,
                "y": 154
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "17af7a41-71bb-41d3-89ad-39a749d80a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 74,
                "offset": 2,
                "shift": 42,
                "w": 37,
                "x": 674,
                "y": 154
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7c2db82f-1659-496d-9bd4-a761488d7cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 628,
                "y": 154
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b043bf7c-1bd9-4c46-958a-16d02f2f00ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 74,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 582,
                "y": 154
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "186ce30c-ebd0-47e2-9693-49d2cd88d1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 574,
                "y": 154
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ac375613-4f9d-4536-a972-8cb24b4fa7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 74,
                "offset": 3,
                "shift": 41,
                "w": 32,
                "x": 540,
                "y": 154
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1e3073d7-15aa-4e1c-8d58-a38ed375e256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 74,
                "offset": 6,
                "shift": 44,
                "w": 35,
                "x": 503,
                "y": 154
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0e663f02-fd4c-4613-b366-b52eba4da309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 74,
                "offset": 6,
                "shift": 42,
                "w": 35,
                "x": 466,
                "y": 154
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a1163629-1c84-4d55-a7f0-5e463396b1ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 74,
                "offset": 6,
                "shift": 65,
                "w": 53,
                "x": 411,
                "y": 154
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f9988c41-318b-429b-abb8-5ef67c90ac6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 690,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1a58a4e4-34c6-4d6d-93a0-08c904f5c38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 634,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "768f11a5-f72a-4ec3-833b-2fc43e2caceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 34,
                "x": 598,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ad32a4bd-608e-4c8c-8789-557444ab837f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 801,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b40892da-39f6-4077-8cec-2162d8ee8c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 35,
                "x": 745,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "631626a2-9e43-4575-8598-d39e04545ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 74,
                "offset": 3,
                "shift": 44,
                "w": 37,
                "x": 706,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b8d33bf9-fc40-4e10-968f-e591e94bdaf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 74,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 662,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d522656d-2f4b-49ab-ac0c-07a002ee1a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 42,
                "x": 618,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "27ea6e5e-eb16-40d1-b8d4-d281b1b16b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 74,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 570,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a8439a8a-de0a-49d2-be38-83ea88ca5ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 74,
                "offset": 1,
                "shift": 63,
                "w": 61,
                "x": 507,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e9356bbd-ec74-4887-aa0f-f1e97bc9a54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 74,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "743c560e-a187-44bb-863b-549040c680fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c6ecce28-6459-4773-880f-0b3506d45a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3ac24e64-cee4-4556-af3b-df07ea1508f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 74,
                "offset": 4,
                "shift": 20,
                "w": 17,
                "x": 782,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c19a05f7-57d1-4b0e-9e84-8cd0429282ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "be8309af-e6ab-4548-a843-a3e67a2a41fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7bf46765-0d88-4d8b-9d88-4f51aef700ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c08628a4-e799-4e42-b322-90e52426d6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 74,
                "offset": -3,
                "shift": 31,
                "w": 38,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9f77d508-beb7-47ac-b4d5-9f763be7be56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d23997b9-554c-4e91-bb7d-d37d4a0c1a54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9325f57a-62d5-4987-8826-9ccb054b1e01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "445ac2ca-1628-4e81-bdc4-e274683ee58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0c9a74ef-ddaa-44db-99a1-27d53652baec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5b00566c-fb99-4958-87d6-a5c6bfe0af4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 74,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "31a9866d-83b0-49c3-948d-4fc31985ed65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 74,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "36642010-3a37-4f6e-849d-65d91e63ffdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 857,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "833aab73-b207-4a77-ab71-e94b1d59c86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 199,
                "y": 78
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "57181cf5-91d5-4c2b-9196-84968338e532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 897,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "064cfd3c-d376-43c7-8eb8-467d1e91e619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 74,
                "offset": -7,
                "shift": 19,
                "w": 20,
                "x": 543,
                "y": 78
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "568e724f-a16d-4245-bc63-d33810af864e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 74,
                "offset": 6,
                "shift": 38,
                "w": 29,
                "x": 512,
                "y": 78
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "490d91be-c775-4342-a833-166ff0e8281c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 74,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 496,
                "y": 78
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ab52d203-ef66-42bc-8bfa-04f6ab6ca8d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 74,
                "offset": 6,
                "shift": 63,
                "w": 52,
                "x": 442,
                "y": 78
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a095c2ae-b55f-49bc-94ec-a8acd731f1ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 406,
                "y": 78
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4d8978af-06a1-4abf-bda9-4799fcb9b484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 366,
                "y": 78
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f1520a08-5556-4cab-9a35-30a60c11ef5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 326,
                "y": 78
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7a4204e9-573b-44ca-8705-46bb883fbdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 286,
                "y": 78
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9bd586a6-b07e-462d-838d-307318717c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 74,
                "offset": 6,
                "shift": 31,
                "w": 23,
                "x": 261,
                "y": 78
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7839e13d-241e-4410-97f3-933edfd2943b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 74,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 565,
                "y": 78
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b4819842-aced-4304-ab14-d1a87de2730c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 74,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 235,
                "y": 78
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9f3ac131-918e-4191-b728-d14d3f123c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 162,
                "y": 78
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8869a948-ac14-4814-8b2a-cbf211ed5c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 74,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 124,
                "y": 78
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5c0933a3-e320-4809-9f6e-f7e2a6aae271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 74,
                "offset": 1,
                "shift": 49,
                "w": 48,
                "x": 74,
                "y": 78
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "82546111-d8e9-4d0a-8d99-e153d27821fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 40,
                "y": 78
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "99a5c5de-2434-482a-b9ac-7c5ebe8ce511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 74,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ff2c430b-8eec-4306-9684-a23325c5f7d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 74,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 963,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "946cb520-b418-4cf0-889c-7b580f17e22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 939,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "118716af-482e-4e94-be6d-eb6a647c2ef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 74,
                "offset": 10,
                "shift": 26,
                "w": 6,
                "x": 931,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c052b80f-e4ef-42b0-9129-370671d0e0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 74,
                "offset": 0,
                "shift": 25,
                "w": 22,
                "x": 907,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c39e0c5d-8955-4323-8b55-840617dcd73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 192,
                "y": 230
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "209661cf-1c7c-4e5a-a31a-ebacb61ff783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 74,
                "offset": 13,
                "shift": 65,
                "w": 39,
                "x": 222,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}