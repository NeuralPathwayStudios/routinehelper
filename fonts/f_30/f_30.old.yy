{
    "id": "dc4d2e5d-4fe0-4abc-8c6e-2464520d0f4b",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_30",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_30\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "19a11e40-94ce-422c-abb2-9d34191ca2c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b5704e81-7975-4bce-bc73-fb28e5941590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 74,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 275,
                "y": 154
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e6b7ee85-2a33-421e-b17f-0f5ccaf749bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 259,
                "y": 154
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0dc03017-545a-4a07-bd09-6192a25a36ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 214,
                "y": 154
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ba6c9657-4380-4d9f-9763-9a6377e96bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 177,
                "y": 154
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "83f8cf37-4cf4-4e36-b247-63ad06a14ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 74,
                "offset": 2,
                "shift": 57,
                "w": 54,
                "x": 121,
                "y": 154
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b5267aaf-52be-4df3-b0b7-b6c5655d1fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 76,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "19a8cbef-44b8-4227-950d-6aea5b7dd0fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 74,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 68,
                "y": 154
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e3705c38-04ab-4ad8-b220-943ca2eb3474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 74,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 52,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fe856eea-2017-4049-9aa0-a2351116ca01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 74,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 36,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "41b288df-31f1-4b4f-809a-8ddf3fa5fa5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 286,
                "y": 154
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4af9ba60-c9a6-4d67-922d-491df4165368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dee659e1-50c8-4bf8-9b65-6e1424d07fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 74,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 964,
                "y": 78
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "171410fc-d834-41fd-ad01-93496869a36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 74,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 940,
                "y": 78
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "45030600-b6a2-40fb-b467-adb2d0bd55ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 74,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 929,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5d5f7b0a-fed0-431b-b2d7-07eed14cd740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 900,
                "y": 78
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ed06ee01-9168-49ad-9f8b-6c7ee3b02f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 866,
                "y": 78
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cf341902-1854-48ca-b98c-30d36c18e3f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 74,
                "offset": 2,
                "shift": 25,
                "w": 17,
                "x": 847,
                "y": 78
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "206a97e0-c67a-4a9b-9a07-3566b8245a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 813,
                "y": 78
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a403497b-2d57-43f9-95fb-5233646843fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 778,
                "y": 78
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "672c8ab1-7c2d-467e-83c1-2af0be3c8761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 41,
                "x": 735,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "eac201b6-f220-4040-b175-360a750c987a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 74,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 975,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "927d3892-3390-4f21-a58f-8d6617440205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 308,
                "y": 154
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5aae0dff-7c16-4b8b-b1f5-b2ac474b2325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 342,
                "y": 154
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "faa0babd-e783-42dd-89ed-7dd19a945f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 377,
                "y": 154
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f9d7af75-d66d-4e9f-9d44-5cab7ac42d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 158,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8c780c58-abbc-46ce-b667-f90c28daf261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 148,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e078928b-54c7-4664-86b9-c59a6e9ee364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 136,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "418431ff-9687-4f02-8d76-d86eec3500c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 102,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f8166b5c-233e-4341-9107-3c44b4685717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 74,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 70,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "31f0bbfb-496e-4dc8-ab26-6da934688e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 36,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c9b3b6a5-c8fe-4fb9-bce7-5d63f72097d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "59aaa710-351d-4034-9539-995524170749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 74,
                "offset": 2,
                "shift": 73,
                "w": 69,
                "x": 931,
                "y": 154
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d950f4e6-0d63-44d1-86c0-461207bd74ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 74,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 882,
                "y": 154
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "807a318e-63e7-4415-811c-184976e17892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 37,
                "x": 843,
                "y": 154
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "64ce048d-8994-44f7-975d-b1a011dbf974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 797,
                "y": 154
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "23f28af4-f8ca-4c1c-b4c1-7da2ce7e4ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 74,
                "offset": 6,
                "shift": 51,
                "w": 42,
                "x": 753,
                "y": 154
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b77b91ac-d70a-4900-9469-9e8e3c9f1225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 38,
                "x": 713,
                "y": 154
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ba302fd1-a238-476b-bffe-1bc2c1c8a703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 74,
                "offset": 2,
                "shift": 42,
                "w": 37,
                "x": 674,
                "y": 154
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b27f5873-6965-47fd-9a45-11c9f3e73767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 628,
                "y": 154
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "05fc481f-ebb4-4da6-a584-5120a816234a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 74,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 582,
                "y": 154
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1eea966f-f457-4ee0-a84e-5f29e3b495f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 574,
                "y": 154
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8df0a8db-301b-417e-b3a9-4cc3165f47bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 74,
                "offset": 3,
                "shift": 41,
                "w": 32,
                "x": 540,
                "y": 154
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "91f2ae5d-800c-4b5d-a3a4-0e88f90d47f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 74,
                "offset": 6,
                "shift": 44,
                "w": 35,
                "x": 503,
                "y": 154
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "22b9bdbf-a6bf-446d-a601-f7359e8b6b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 74,
                "offset": 6,
                "shift": 42,
                "w": 35,
                "x": 466,
                "y": 154
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "52915dd3-8336-4d34-b559-6f81a3e11b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 74,
                "offset": 6,
                "shift": 65,
                "w": 53,
                "x": 411,
                "y": 154
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b755cd28-db04-4627-a9bc-7f5931faf919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 690,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0c2b3cd6-3d00-4f35-a111-46825579147a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 634,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "94dc3dc2-5895-47ba-b6a1-20c72ca1a557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 34,
                "x": 598,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c8de1c0c-51a8-4863-8619-c6fcdd17a356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 801,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3a4fc615-1d24-4df1-9869-232ab77f0711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 35,
                "x": 745,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d7ece848-225a-4dd2-ad1b-5dcd81df23a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 74,
                "offset": 3,
                "shift": 44,
                "w": 37,
                "x": 706,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e7413863-15f5-4490-b5af-149c0a10c4a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 74,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 662,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "eafb070b-a056-4475-a855-cf7fe402b73d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 42,
                "x": 618,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5929639e-a6db-4338-bd7d-7c75adadac04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 74,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 570,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "42eafcfd-f027-45ac-a9b0-5f4b7f01751a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 74,
                "offset": 1,
                "shift": 63,
                "w": 61,
                "x": 507,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bbbf5cbb-24d8-47d3-9779-e50f331836e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 74,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "320d9164-f1d5-4987-afcb-c6a79c8cf371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "46d47077-a545-406f-847d-f77068d08b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "20f92029-eb40-4258-867a-8e1dc88f4d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 74,
                "offset": 4,
                "shift": 20,
                "w": 17,
                "x": 782,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "756f1681-5c9c-4cc0-bf5b-b91bd6497d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "46b048e2-dba0-4481-bd40-e3e23d1bdfdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fecc89ce-1593-496a-b17a-c9b7d461d5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "007003df-3433-4e9a-b89d-3f49cb439860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 74,
                "offset": -3,
                "shift": 31,
                "w": 38,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "458c75b0-fa80-47cf-a401-fa2fe5c15542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9b2b1e65-41c7-4819-a3e6-49f97fa984eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "da22da68-a2fc-47bf-b1e7-36a351870a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "07946c9c-d68c-4809-a1e6-73253d45df86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f99c5177-a681-4718-9302-d9bdaabf76bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "77b1dbb5-2843-48ed-abf0-a6df186b646d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 74,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a1779418-c1d2-494d-a03b-b1cd32af83fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 74,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "41f19c20-3b53-452b-a7de-e55e5041caf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 857,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d842a0b3-2847-4cfc-939a-9dea634b15f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 199,
                "y": 78
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bc012ec0-d97a-4220-8ae2-de595a27e364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 897,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9db4c8ea-0634-442b-8cca-a22481430aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 74,
                "offset": -7,
                "shift": 19,
                "w": 20,
                "x": 543,
                "y": 78
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "aef5692f-cfd7-477a-933f-fbbdbc02e31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 74,
                "offset": 6,
                "shift": 38,
                "w": 29,
                "x": 512,
                "y": 78
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "776c847f-5e60-4069-9143-6d1c5326295b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 74,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 496,
                "y": 78
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1b3b5731-680f-4902-85fa-fe1ebd6eb5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 74,
                "offset": 6,
                "shift": 63,
                "w": 52,
                "x": 442,
                "y": 78
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0f4d1d97-cda6-4cc9-b25b-19445a7ff9da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 406,
                "y": 78
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2cbda015-de74-4b97-9ae4-196dc1931f04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 366,
                "y": 78
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b7a411ca-3015-471d-8f27-5198fd0ba794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 326,
                "y": 78
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5e706bb9-4f3b-4fff-b71b-31e754eba881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 286,
                "y": 78
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ccbc82b7-70f8-46c6-a737-d7bbbc5d3904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 74,
                "offset": 6,
                "shift": 31,
                "w": 23,
                "x": 261,
                "y": 78
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dba3e246-da1d-4089-8047-1a41393c41d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 74,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 565,
                "y": 78
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "df59806b-eae6-40ff-92ff-ca8f80957128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 74,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 235,
                "y": 78
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "61c83310-995d-4275-8ab8-229b8f6151e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 162,
                "y": 78
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a28b2503-1a93-49b1-b49b-dbe1987f7e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 74,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 124,
                "y": 78
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "18d651f3-cd14-406e-ba57-6e3849712f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 74,
                "offset": 1,
                "shift": 49,
                "w": 48,
                "x": 74,
                "y": 78
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "88d8651c-b230-46fa-8522-b9f58da4a5fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 40,
                "y": 78
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c137b093-f350-4ff5-9f76-80ccc62fd3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 74,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4497948b-50eb-49b4-9dc8-32b55cea0d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 74,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 963,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "55c693ad-2c02-4161-acf0-4fac909dc618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 939,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4162ef79-3727-49bd-9291-4b04a97737d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 74,
                "offset": 10,
                "shift": 26,
                "w": 6,
                "x": 931,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7037e823-2413-47ad-8d11-d159205ab64f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 74,
                "offset": 0,
                "shift": 25,
                "w": 22,
                "x": 907,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d34df24e-01e4-4727-81b8-a58805321b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 192,
                "y": 230
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "abbfca49-8991-47df-96d9-d2639e2e9cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 74,
                "offset": 13,
                "shift": 65,
                "w": 39,
                "x": 222,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}