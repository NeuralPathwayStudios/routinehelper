{
    "id": "270fcb8f-c217-48eb-bdb2-6f3c2b6022d3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_40b",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_40b\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "aca52570-e837-4fa9-840e-37034444dbfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 60,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dc1f2423-eeea-45a2-b5e6-0fcc7c4e8bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 60,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 470,
                "y": 188
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9712c195-7789-46a8-a4ec-bed5bc1553b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 60,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 455,
                "y": 188
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1d904032-c4f4-4e84-949f-3c619d1f47d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 60,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 419,
                "y": 188
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2683863a-b817-4718-a713-8cb81be1a513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 60,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 387,
                "y": 188
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d943ee93-2e9f-43f4-a54f-b6ccda37992b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 60,
                "offset": 1,
                "shift": 46,
                "w": 44,
                "x": 341,
                "y": 188
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "77d84235-facf-4d25-ac81-42a9a49e656d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 60,
                "offset": 2,
                "shift": 39,
                "w": 36,
                "x": 303,
                "y": 188
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8287e723-5e68-4b15-8a77-f90d13d705d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 60,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 295,
                "y": 188
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0a2fcde6-11ec-4036-85da-f3acdd8f0ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 60,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 280,
                "y": 188
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a9cd45ea-62e9-4a0c-9f16-960223686db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 60,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 266,
                "y": 188
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "22d719cd-0107-4867-a5e6-0f31f7b618ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 481,
                "y": 188
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0589b8c7-19e8-4b86-820a-27af1bcac841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 238,
                "y": 188
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d08a3032-3c30-4672-9c33-d446b858f1a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 60,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 197,
                "y": 188
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "65d15d2c-47cc-4bef-8024-d0264e498fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 176,
                "y": 188
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3d19f5a6-d6d9-413f-a04f-92e3a30b4011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 60,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 165,
                "y": 188
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "23bcd138-d8d0-496f-a1df-c606773c8ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 140,
                "y": 188
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d188a8af-f6f7-495a-b745-a03b5722f99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 60,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 111,
                "y": 188
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c4e42762-16ff-477f-874f-13a0e584e96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 15,
                "x": 94,
                "y": 188
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2ae628fa-1f46-45ef-b814-92cb2f6c9d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 66,
                "y": 188
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c0c23b4c-fd3d-4cce-99fa-1fc5a4eb38a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 60,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 36,
                "y": 188
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2d0f362b-95f1-46f8-a3f7-a177372fd421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 60,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 2,
                "y": 188
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0b411a54-9af0-4181-ae5f-7f83fe451915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 60,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 208,
                "y": 188
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9acb9585-a90d-4d19-89ca-0c83897f579d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 2,
                "y": 250
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ed3ca5a0-4094-4b09-a0f4-7be0613db184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 60,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 30,
                "y": 250
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "202802b3-f85d-4be5-8309-35a675d2f3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 59,
                "y": 250
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "60b26257-fbd7-44f0-9eec-5ffe24f938a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 60,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 232,
                "y": 312
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "86c1f555-a052-4fbe-be1b-5571eda19887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 60,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 221,
                "y": 312
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "27cf60b3-1482-4b9c-99a6-0b80febcf89d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 60,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 209,
                "y": 312
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4053c4cd-b1e4-446c-af59-6ab22c5581e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 181,
                "y": 312
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9d3f0df9-7a76-4e00-b8d4-b9cddefdaabf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 60,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 155,
                "y": 312
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0edd92b8-14d9-4260-a2ae-d4e01ee34959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 127,
                "y": 312
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "09990be1-3759-41ca-8bef-12462a81e4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 99,
                "y": 312
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "abd6b35d-a267-4c4d-b348-a0e27de7367a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 60,
                "offset": 2,
                "shift": 58,
                "w": 55,
                "x": 42,
                "y": 312
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6de4e9d8-facd-46be-92c2-9024b47b8664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 60,
                "offset": 0,
                "shift": 37,
                "w": 38,
                "x": 2,
                "y": 312
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "91dc4041-37cf-4e8d-8ff6-9ed35f7689be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 60,
                "offset": 5,
                "shift": 39,
                "w": 31,
                "x": 445,
                "y": 250
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9b81f34a-97e6-4136-b5a7-fc594417bfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 60,
                "offset": 3,
                "shift": 41,
                "w": 36,
                "x": 407,
                "y": 250
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6418794c-f0b8-4b0e-9dcc-70a0da5592b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 60,
                "offset": 5,
                "shift": 42,
                "w": 34,
                "x": 371,
                "y": 250
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "68a33aeb-9387-4f2a-854a-d76c3449a000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 60,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 339,
                "y": 250
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ce691f4f-0297-4baf-8959-522008a7e0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 60,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 306,
                "y": 250
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bf858809-a040-4ed0-a0b9-6abe19d5ff97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 60,
                "offset": 3,
                "shift": 41,
                "w": 36,
                "x": 268,
                "y": 250
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "85832a80-36ae-4260-8274-d8881ba0d9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 60,
                "offset": 5,
                "shift": 46,
                "w": 37,
                "x": 229,
                "y": 250
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5e05472b-eb2a-4653-a9ab-b3c5b8cba49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 60,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 221,
                "y": 250
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "31266c3f-a4d2-4cc3-8806-d9abf3303a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 60,
                "offset": 2,
                "shift": 33,
                "w": 27,
                "x": 192,
                "y": 250
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ecfc3540-df9a-41e8-9843-c2a29e5f8142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 60,
                "offset": 5,
                "shift": 36,
                "w": 29,
                "x": 161,
                "y": 250
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7c0bab12-3cfc-4d60-a311-2e69ec2ccaa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 60,
                "offset": 5,
                "shift": 34,
                "w": 28,
                "x": 131,
                "y": 250
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "543aea4e-171f-4720-a37e-69d99519681e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 60,
                "offset": 5,
                "shift": 52,
                "w": 43,
                "x": 86,
                "y": 250
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c0c23044-f735-4cf0-9326-642425968b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 60,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 442,
                "y": 126
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1dd588c2-e82d-412c-973e-94a67ad81eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 60,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 397,
                "y": 126
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "616988db-6412-4e49-aba7-305f1e2c913a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 60,
                "offset": 5,
                "shift": 33,
                "w": 28,
                "x": 367,
                "y": 126
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "61708556-43ae-46e6-9e0d-cc8e9c25d1c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 60,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 199,
                "y": 64
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "921cdfd8-9cff-4a54-a0dc-dafd20f59bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 60,
                "offset": 5,
                "shift": 34,
                "w": 29,
                "x": 151,
                "y": 64
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3d9bd170-6d41-4d46-a08d-78bab8ccb178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 60,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 117,
                "y": 64
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "aa5b266a-52f7-41f0-ae50-66afdb832dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 80,
                "y": 64
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "30af16ff-370d-44c5-a869-0a1bbdf1568c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 60,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 43,
                "y": 64
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "37599fe6-817c-48cb-a985-28ea8b530600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 60,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "20836601-d838-4a3b-a34e-c9f10372bbc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 60,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "83187a3e-c3f0-4ad9-ac22-aa23b1ee055c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 60,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8f729ded-2d0f-4416-9854-eb5a2cc2091e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 60,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "54a75b21-c373-40fe-ad6c-0e9d6614b2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 60,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "70d70dde-909e-4e69-9715-a00e14a16108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 60,
                "offset": 3,
                "shift": 17,
                "w": 15,
                "x": 182,
                "y": 64
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d243b771-cbde-4eff-bc97-a39ff0732705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 287,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "527113ad-9f87-430c-8ac3-27d03756e05b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 60,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "292e8637-e93a-46b9-8cf0-87d54656ee74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dd97424f-1069-4962-b32d-e4cf435c8f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 60,
                "offset": -3,
                "shift": 25,
                "w": 32,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bbb9e543-207a-4d8d-b996-e80fec285cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 60,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6b16cb27-b560-4c06-8b8a-8baf85c43bdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8a3cad09-b4c5-4858-bf3a-1dd631437a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 60,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "96c218eb-c1bb-44a6-8d85-8af4c9bd33ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 60,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "223505c7-5547-4cb5-bfc5-56293e775ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 60,
                "offset": 3,
                "shift": 37,
                "w": 30,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5ea283c3-c3ca-466b-a877-dce441bdbfb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 60,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "54c7afbb-c0d3-42c8-a45a-ce9abf1e0398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 60,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "67db7f42-98bd-4efa-9b95-7d2d16fa9efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 244,
                "y": 64
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7756b291-b9f2-4931-9328-23d5abb0cfb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 29,
                "x": 32,
                "y": 126
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5fc3eb57-4e8f-4f6c-8b46-d962f63623f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 60,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 276,
                "y": 64
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8459ea89-4373-4040-b47b-9b6180ecba3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 60,
                "offset": -6,
                "shift": 15,
                "w": 17,
                "x": 321,
                "y": 126
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "812c5cc2-b5f1-4a3e-9ace-83e7b6551846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 60,
                "offset": 4,
                "shift": 30,
                "w": 24,
                "x": 295,
                "y": 126
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "77e52b2b-da41-4f37-89db-d5d1e787ada8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 60,
                "offset": 3,
                "shift": 16,
                "w": 13,
                "x": 280,
                "y": 126
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d88a63aa-2f45-4fed-9a17-c2a6298722e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 60,
                "offset": 4,
                "shift": 50,
                "w": 42,
                "x": 236,
                "y": 126
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "29ef9d6e-37c4-4bc7-b974-0d98b5e27242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 29,
                "x": 205,
                "y": 126
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b5e5c599-3888-48f7-b64a-48af84c76006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 173,
                "y": 126
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "51633b70-ee3b-46f3-b982-4cf9a1982a38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 60,
                "offset": 4,
                "shift": 37,
                "w": 31,
                "x": 140,
                "y": 126
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c434dd0d-aeb4-460a-8291-cf8dc37c626d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 60,
                "offset": 3,
                "shift": 37,
                "w": 30,
                "x": 108,
                "y": 126
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "789c5e6f-5c6e-468a-9ab4-64bea8458d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 60,
                "offset": 4,
                "shift": 26,
                "w": 22,
                "x": 84,
                "y": 126
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e4bc29eb-74b9-4bb3-aeaf-d35b1eabd155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 340,
                "y": 126
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a0dbde3b-2eac-4954-b81f-84f6aa0e59d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 63,
                "y": 126
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "be5660cb-5b01-46bb-8c11-3cfb01b10ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "22c17771-012e-4366-b8ae-9e315649e8ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 60,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 464,
                "y": 64
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2018a58b-525d-4ecb-977a-169e194442d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 60,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 423,
                "y": 64
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "676b6c9d-cc94-4374-ad44-012790e8d133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 60,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 395,
                "y": 64
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3cac65e4-dfb0-49c2-9619-b5f7d3c2eb1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 60,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 363,
                "y": 64
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6db2a070-d975-4ada-b17e-c99f76a2b8b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 336,
                "y": 64
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c28359f9-6780-41ba-a1ec-a54683662709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 315,
                "y": 64
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0b845949-ce58-43c7-8560-79ef035569a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 60,
                "offset": 7,
                "shift": 21,
                "w": 7,
                "x": 306,
                "y": 64
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e880f71e-516c-4492-a839-3a1ee3a814b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 286,
                "y": 64
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "63524199-b190-4b83-a0b9-55087b1bf0b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 260,
                "y": 312
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "22cd6ee2-e98b-44be-9f79-ab42aaacd56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 60,
                "offset": 10,
                "shift": 51,
                "w": 31,
                "x": 285,
                "y": 312
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 40,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}