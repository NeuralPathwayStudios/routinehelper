{
    "id": "82647b4d-0136-4556-a23a-2e8365109bb4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_501",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\f_501\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bfc968d4-bd8a-4445-ad4b-916ff97595bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "333bbd21-86c1-441d-b8bd-fe73673cc853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 74,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 275,
                "y": 154
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f05b6923-46fd-41db-a2a7-82c303534ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 259,
                "y": 154
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5ed2622e-b132-4ec0-9aeb-9f4011a7dbbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 214,
                "y": 154
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "452e0fbf-69ea-48ac-ae98-fca568ad6bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 177,
                "y": 154
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e5f775b8-7b38-416f-ad29-88bfb85e7a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 74,
                "offset": 2,
                "shift": 57,
                "w": 54,
                "x": 121,
                "y": 154
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f2006dbd-10c0-4faf-8c52-a6a9964ab8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 76,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cbb7d16a-d860-4395-a89c-dee1889ed19d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 74,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 68,
                "y": 154
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "149b741a-7ece-4de3-97b7-5ae79bb83fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 74,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 52,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f185393c-9efd-4cbd-b74d-29284e366f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 74,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 36,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0bdf488a-c49a-47e2-bac5-069642f20b59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 286,
                "y": 154
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b1a64d8e-3450-42a8-b300-3d7108fca002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "37e41cab-8b5a-47c2-a7a9-194c3172d8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 74,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 964,
                "y": 78
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b947f186-c0c4-4d81-8d34-740f7803418f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 74,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 940,
                "y": 78
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dc87ab5e-d0a5-4973-b6f0-88adc29cc354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 74,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 929,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "63fdf7f3-e388-46c1-b903-473d54ffcd29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 900,
                "y": 78
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0304d2e9-a988-4320-83a1-46e89e9cfdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 866,
                "y": 78
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cd703763-6883-4a27-b0c9-23121554d873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 74,
                "offset": 2,
                "shift": 25,
                "w": 17,
                "x": 847,
                "y": 78
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cff9aae4-3cfb-4fab-aec7-d51570a17f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 813,
                "y": 78
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "179b1ab4-b228-41f1-8347-0137085fb3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 778,
                "y": 78
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6eb49bfd-6449-4d23-b27b-88ab7d331120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 41,
                "x": 735,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b8ff5cff-5edd-4e57-9859-77abf4b8c373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 74,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 975,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ab043243-e5bb-4c2d-ac4d-9d00fddac3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 308,
                "y": 154
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f721ebed-eeea-4dc1-9fe3-4f4fc4faf528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 342,
                "y": 154
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f52a0fff-44e9-4e05-851d-b17dc642890c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 377,
                "y": 154
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3ecd975c-405c-4e5d-8a32-aae1ec13309e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 158,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2773f355-75d5-4ae7-8462-bd42c574ad03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 148,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "954dc266-6e09-41fc-9073-fdf0bf191d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 136,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4d9122a9-995d-4c1c-a58c-c0f8c1611c89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 102,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6e8f926d-85ca-48d0-959b-56e57ce61e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 74,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 70,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "317fe5f2-f8b4-467d-a8e0-9ca758de0603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 36,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1eb46c55-429f-45b0-8859-8c4454ec18e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c14ec914-a725-4fce-92ef-dae550eb81a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 74,
                "offset": 2,
                "shift": 73,
                "w": 69,
                "x": 931,
                "y": 154
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "96bf2924-7771-43cf-8eb4-8ee38a359327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 74,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 882,
                "y": 154
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f862801f-bc71-4d62-8212-da8e7f428bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 37,
                "x": 843,
                "y": 154
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "370fdb30-8850-47d2-b3a7-0793952d4bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 797,
                "y": 154
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b0e63207-dad9-4b41-bffb-fe21b4ae9116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 74,
                "offset": 6,
                "shift": 51,
                "w": 42,
                "x": 753,
                "y": 154
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e1ea12d6-ba86-4864-ad96-64ab2380532b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 38,
                "x": 713,
                "y": 154
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "09672ee6-5ab8-4d95-bbbb-cc4eb663a669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 74,
                "offset": 2,
                "shift": 42,
                "w": 37,
                "x": 674,
                "y": 154
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ee52bfeb-31f3-4ef3-a3a6-a540e9844859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 628,
                "y": 154
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d1aee602-955e-4588-9b00-08afe1ed034d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 74,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 582,
                "y": 154
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "44bb43a4-f344-46cb-b495-6a26bc0638f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 574,
                "y": 154
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "925bfe10-fb70-4b85-932a-971371ae8adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 74,
                "offset": 3,
                "shift": 41,
                "w": 32,
                "x": 540,
                "y": 154
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "44ae12e6-28c8-4c18-b5e4-03d941c45f46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 74,
                "offset": 6,
                "shift": 44,
                "w": 35,
                "x": 503,
                "y": 154
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d0a1dcfe-deae-4c2c-97cc-1cab2a5520f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 74,
                "offset": 6,
                "shift": 42,
                "w": 35,
                "x": 466,
                "y": 154
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d5ac3f3c-4e36-470b-9bda-ff8b78c46ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 74,
                "offset": 6,
                "shift": 65,
                "w": 53,
                "x": 411,
                "y": 154
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f31d31ef-ba6a-40be-94dc-0ee8eb3a1540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 690,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "12b572a8-c3f5-47e2-ae2e-03cfc10e4143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 634,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b40ebb6f-4ded-426b-adbf-6acf33d4140c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 34,
                "x": 598,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e4cdbb7c-170c-47be-a710-7f084ea29559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 801,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8d2f54d4-6856-4005-a7d9-49813a5e4588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 35,
                "x": 745,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e432a989-d2d4-4b8c-89e4-a5e01c0b6e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 74,
                "offset": 3,
                "shift": 44,
                "w": 37,
                "x": 706,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "878367e3-a26b-4c99-a547-1aa04d620bc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 74,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 662,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "722c826d-e868-44cb-bddf-74db8539c51a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 42,
                "x": 618,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a113107-d303-414d-9e27-ef05fac82389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 74,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 570,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "610bcaee-63ea-4904-a344-9cc0ef2353a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 74,
                "offset": 1,
                "shift": 63,
                "w": 61,
                "x": 507,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e4d924cf-3184-4a7a-90f5-20b914aa1b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 74,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1ecf2a61-dfa0-42cc-8897-431102cf6c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9a697ed7-f0be-4f36-9b3e-a2f6b0ab4bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "73f0432d-de8b-42aa-90c3-fc3b1eeb147f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 74,
                "offset": 4,
                "shift": 20,
                "w": 17,
                "x": 782,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "91edb2d9-bdf8-47ba-b2fd-c999114c5f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4347592d-27f6-41cb-8eb9-3d25cd079b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d9017bc5-da46-48a7-8efe-f9c65cd4815f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "91de8aff-93e0-47b5-8cc6-1bdbb321e4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 74,
                "offset": -3,
                "shift": 31,
                "w": 38,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ca9a0800-3a76-4490-b045-0cd0a0dba812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5ae55e56-66c1-4fa3-b364-e2141f98d8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5a49a106-29e5-4fd5-be77-801fa264a869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4f389279-23a2-48a4-ae82-4caddd0aa152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "112b29be-f478-4062-be25-78d8b2534de9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "08929b62-e063-49b4-8cc5-c3d5aaaf3e24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 74,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fcbf106b-2740-4add-bddd-d202444bce5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 74,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9e7711a8-852b-40ba-a5e3-b3e1f7b89d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 857,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b993d18d-5b5b-439e-92a6-b18a700d8eba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 199,
                "y": 78
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2bfa704d-97fb-4e39-a3b0-5f7fbfc739d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 897,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d10b675b-b403-4aa1-9fa2-c0c311df5bba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 74,
                "offset": -7,
                "shift": 19,
                "w": 20,
                "x": 543,
                "y": 78
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "94e3b4a9-9211-4cc9-9885-7c3a02048b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 74,
                "offset": 6,
                "shift": 38,
                "w": 29,
                "x": 512,
                "y": 78
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cc2a1c1c-846f-4751-b2b0-0bccbdea73e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 74,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 496,
                "y": 78
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "20663f76-1117-49d6-8dbb-ad8bdf6b526b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 74,
                "offset": 6,
                "shift": 63,
                "w": 52,
                "x": 442,
                "y": 78
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ab716148-d052-4501-b59b-d1ec45cdbc33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 406,
                "y": 78
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6f3ee6e8-9385-423e-850e-f25fc75a3d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 366,
                "y": 78
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "42a30a93-146c-4f0d-9a0a-7a3a4edf83a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 326,
                "y": 78
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "252ef266-2a26-49a5-b2e3-260413a9df23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 286,
                "y": 78
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e8699dbc-692e-49c1-ac63-cbc9addba8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 74,
                "offset": 6,
                "shift": 31,
                "w": 23,
                "x": 261,
                "y": 78
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "02e5eb08-6bf9-4a54-83b8-2cfeb6766166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 74,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 565,
                "y": 78
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "724ee2d3-065c-49a4-9eb7-554eb39814a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 74,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 235,
                "y": 78
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d3614429-ae5e-45c3-83c3-102fcb78871a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 162,
                "y": 78
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d9f40da0-9569-48e6-88e8-c15f8b70ea7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 74,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 124,
                "y": 78
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c9281091-f934-4e5d-ae06-545ef966757d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 74,
                "offset": 1,
                "shift": 49,
                "w": 48,
                "x": 74,
                "y": 78
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ee881b59-5641-4122-925d-530abbef0f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 40,
                "y": 78
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "55f426ad-8224-4f66-8860-fe6f38e3a799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 74,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "91c8018e-f021-4c38-99ce-1f5776f8d7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 74,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 963,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "96369552-4e4e-4743-9b72-55ae2131f95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 939,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7982abf1-4711-4ea8-b8ad-0d044607a522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 74,
                "offset": 10,
                "shift": 26,
                "w": 6,
                "x": 931,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f5491713-a2aa-4686-9cf0-042a8875f8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 74,
                "offset": 0,
                "shift": 25,
                "w": 22,
                "x": 907,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "eef96a5d-2fec-46d7-84dc-684e23865e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 192,
                "y": 230
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "580791ea-14f9-4467-8d39-73703a337232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 74,
                "offset": 13,
                "shift": 65,
                "w": 39,
                "x": 222,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}