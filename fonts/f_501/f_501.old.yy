{
    "id": "b7523361-18d8-4879-8d86-fcc45bdc5637",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_50",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_50\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d7add221-1ef7-4189-b94d-94e7eecc610e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cec1be93-d32a-4fcc-9c99-cba01f92b913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 74,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 275,
                "y": 154
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0b8f0a8f-5921-4469-988e-31417f026e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 259,
                "y": 154
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d3462d87-60ca-4696-b7ef-cb276d84e427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 214,
                "y": 154
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2d9f4ced-73f2-4bef-98ee-69f15c643d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 74,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 177,
                "y": 154
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "27c3900d-d606-4074-b3ee-c1e737a7ce88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 74,
                "offset": 2,
                "shift": 57,
                "w": 54,
                "x": 121,
                "y": 154
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a8edb5b4-6595-44cf-8721-1a3e6aa5f78e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 76,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dfcca251-d711-4a50-8578-12b2d7d09843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 74,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 68,
                "y": 154
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7ac04069-0993-4175-8788-cb41f0962947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 74,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 52,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ebd37922-e70d-41be-bd44-42c37e8937d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 74,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 36,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2adecd5d-3d43-4e7f-9e42-018bc01e22a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 286,
                "y": 154
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9b642265-fdc1-4826-a037-2a45d3ec9b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3a48a1ac-4779-48d8-b4ef-7bb1bf4aeb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 74,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 964,
                "y": 78
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ffd13270-635e-45e5-bbb7-39d13a8d4286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 74,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 940,
                "y": 78
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "418d05c2-c5b8-4e89-9a6a-b7e815628723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 74,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 929,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6bd71b9f-d82f-4cd7-82cf-8666ba69dde4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 900,
                "y": 78
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2c2fd4d4-5909-4f24-b726-2f96f89ab4b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 866,
                "y": 78
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f83e639b-7054-43cb-9f2a-8cb2827ed4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 74,
                "offset": 2,
                "shift": 25,
                "w": 17,
                "x": 847,
                "y": 78
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4f5e94f6-2cd8-482b-94fe-e027b1aa7be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 813,
                "y": 78
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "89c0c255-d174-4e95-aa58-dda9c6f5455c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 778,
                "y": 78
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "650a34c6-e659-41d1-bef3-3d6ed07608ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 41,
                "x": 735,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c9086476-fa27-4764-b2b3-cd66a953570e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 74,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 975,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d4bb7fa0-5cb7-4fa8-b697-70e032bcd300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 74,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 308,
                "y": 154
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a62e078d-0a61-467d-aa8a-167dcecbf7df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 74,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 342,
                "y": 154
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b8ba18df-cb7b-4403-b123-0a1a1d32a16f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 377,
                "y": 154
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "980050d1-2b8f-4669-ad88-990f05770757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 74,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 158,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1f9e27cd-a7a6-4e28-a7d2-4efe4ad3873b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 148,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8cf6360e-a48c-4c17-ad73-6787eec3fb0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 74,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 136,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8e4eccf2-4e80-427a-9420-f576a476f5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 102,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ce9e8d2b-d20c-4445-98b2-cf7f6fadfcca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 74,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 70,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e511f2ac-5b17-4384-a097-2fd777cbffcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 36,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "54b861fc-704e-4334-b759-c62889030e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 74,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e78f2fb4-734d-45ec-acf2-9f3726c13a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 74,
                "offset": 2,
                "shift": 73,
                "w": 69,
                "x": 931,
                "y": 154
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "231d280f-0796-4902-a313-2fa8da5a583e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 74,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 882,
                "y": 154
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2b35d66c-6703-42e1-969f-679f4755c1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 37,
                "x": 843,
                "y": 154
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "84e3458b-2542-489f-b11b-0e1cdb8e69a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 797,
                "y": 154
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1b05d50f-e943-4c1b-b7ed-1c16823e7ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 74,
                "offset": 6,
                "shift": 51,
                "w": 42,
                "x": 753,
                "y": 154
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3d5e04f6-76b4-4e2f-8978-f9bb2b91640f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 74,
                "offset": 6,
                "shift": 46,
                "w": 38,
                "x": 713,
                "y": 154
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b2f0c16e-3ada-4939-b270-9f9a794277b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 74,
                "offset": 2,
                "shift": 42,
                "w": 37,
                "x": 674,
                "y": 154
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "36751bb2-9ab7-472d-bcd0-df3bdcc9154c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 74,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 628,
                "y": 154
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "796a8672-9830-4fac-82eb-2f7cdffcb008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 74,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 582,
                "y": 154
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b714abba-4f05-42c2-afbf-720b997abe47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 574,
                "y": 154
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "311f63e2-0ce0-4d7e-9386-873bf1074a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 74,
                "offset": 3,
                "shift": 41,
                "w": 32,
                "x": 540,
                "y": 154
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c1ff20b6-c2e0-4dd6-a4e9-bf9b4346d038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 74,
                "offset": 6,
                "shift": 44,
                "w": 35,
                "x": 503,
                "y": 154
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6eb3dac1-29a1-42d8-b55e-77d75600b82e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 74,
                "offset": 6,
                "shift": 42,
                "w": 35,
                "x": 466,
                "y": 154
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f05a064e-4d95-49d0-87c7-4353554a09a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 74,
                "offset": 6,
                "shift": 65,
                "w": 53,
                "x": 411,
                "y": 154
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d50234d0-8956-47cd-9a6d-8589bcf449e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 690,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "319e1832-ef56-44ed-a79c-a05208a16ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 634,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "03733078-ee39-4f4b-bcd0-fa80b148fb02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 34,
                "x": 598,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c30235e9-ce8b-45b2-b4e2-081dbff1bb87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 74,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 801,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "40da93df-15d5-4546-b309-89fc2e99ea02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 74,
                "offset": 6,
                "shift": 41,
                "w": 35,
                "x": 745,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d9c7745f-c5ee-4451-b90e-38e2a205a01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 74,
                "offset": 3,
                "shift": 44,
                "w": 37,
                "x": 706,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1ffe5bfa-9605-433d-a5d6-98f91211ac11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 74,
                "offset": 1,
                "shift": 44,
                "w": 42,
                "x": 662,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2ad25655-e7ab-4272-8daf-c03b69aae1a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 74,
                "offset": 6,
                "shift": 55,
                "w": 42,
                "x": 618,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "67be7a36-c292-471d-abe0-54a76998a46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 74,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 570,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "92dfe256-22d3-4073-8243-51f1af418305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 74,
                "offset": 1,
                "shift": 63,
                "w": 61,
                "x": 507,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4f22aa87-97a4-4467-9004-366890d4e724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 74,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "68747560-8145-4350-913c-24177135cc0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 74,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "aefc1aeb-efb4-451a-9622-6001a066bafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 74,
                "offset": 3,
                "shift": 48,
                "w": 43,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9bed20d0-dbb7-41ab-a50e-5257c4613381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 74,
                "offset": 4,
                "shift": 20,
                "w": 17,
                "x": 782,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "61c6a498-33d3-4669-b89d-717ea18d6219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "98a030a7-b3c6-4401-a583-95dbdf3f8582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 74,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "90de86f9-5597-4369-878b-002f1f237e24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ffbdd69c-794d-4ad2-bfcf-89784feb04fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 74,
                "offset": -3,
                "shift": 31,
                "w": 38,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c915948b-f3a3-4412-b893-c35355606741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 74,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "491e4e62-fabe-4288-833a-148ec102f91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "43d0d414-7ae7-4128-bca5-c9a1a944bb4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a1da5783-54be-46f8-b050-f604a0f14afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 74,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c78da6da-9154-441d-9536-420768cf3693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c39b6420-c4a2-4027-8dc1-24f4ac95dffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 74,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "23a9c3cd-6b0f-4e27-be92-3d24995dc8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 74,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "07a55408-bba9-4f4c-bcb4-ba9d17b23df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 857,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a86a6abd-6626-4a86-9e70-2d7dc5b97c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 199,
                "y": 78
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ecc08087-6e5b-48a2-bca2-c9af18804623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 74,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 897,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c8c8abbd-1e06-4456-8404-d7999135f463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 74,
                "offset": -7,
                "shift": 19,
                "w": 20,
                "x": 543,
                "y": 78
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "51d671c7-bfa6-4b09-90cd-10aeebd5a0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 74,
                "offset": 6,
                "shift": 38,
                "w": 29,
                "x": 512,
                "y": 78
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e63b6d0d-6e72-4ddd-b929-3579fad90ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 74,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 496,
                "y": 78
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "36bf2fc2-189c-4083-935e-b24235012bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 74,
                "offset": 6,
                "shift": 63,
                "w": 52,
                "x": 442,
                "y": 78
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a09e820e-a169-4ff4-a4c5-3aee8a696c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 74,
                "offset": 6,
                "shift": 45,
                "w": 34,
                "x": 406,
                "y": 78
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "269769f6-7cf0-4e2a-b85a-c77891112ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 4,
                "shift": 45,
                "w": 38,
                "x": 366,
                "y": 78
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "14e0427c-8a45-406a-b64d-a2a71ad6d44d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 74,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 326,
                "y": 78
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3ddcef45-dc55-4c97-a05a-bdb4d9cf20e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 74,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 286,
                "y": 78
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fd881ac4-c9c6-4c9f-8a79-8e4ddd47e736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 74,
                "offset": 6,
                "shift": 31,
                "w": 23,
                "x": 261,
                "y": 78
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "181c3dfa-48b4-4370-97ea-45c09f9c271d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 74,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 565,
                "y": 78
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b9c20157-36cb-4fb4-bfb7-0137111b90a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 74,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 235,
                "y": 78
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e3da84c7-d61b-4770-bbcd-212ae03f6bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 162,
                "y": 78
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "452dc7ff-eca3-4ff0-83b2-3fd18e502870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 74,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 124,
                "y": 78
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2ec61c05-20c9-45b1-938c-dd34b7c724d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 74,
                "offset": 1,
                "shift": 49,
                "w": 48,
                "x": 74,
                "y": 78
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "67e1faca-4743-4baf-894e-49608e71c6b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 40,
                "y": 78
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b7c7350f-5667-4718-a77c-11c09c8dbd95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 74,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "af4deea5-1ac0-4dd8-958f-ace6a2644859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 74,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 963,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e4aa79a6-097d-4452-8224-14f11a1b18c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 74,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 939,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ae576f62-c917-4b85-97e6-d5cbe0e8241e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 74,
                "offset": 10,
                "shift": 26,
                "w": 6,
                "x": 931,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9de32ace-8bf9-41ad-8564-cb47e8e75894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 74,
                "offset": 0,
                "shift": 25,
                "w": 22,
                "x": 907,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "33048f0c-18cf-4fe2-83ee-e877af7cb0f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 74,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 192,
                "y": 230
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f5e0467d-8ca3-47d0-beb3-b5980de56576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 74,
                "offset": 13,
                "shift": 65,
                "w": 39,
                "x": 222,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}