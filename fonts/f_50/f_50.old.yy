{
    "id": "b7523361-18d8-4879-8d86-fcc45bdc5637",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_50",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_50\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c5c273f3-7a0e-4e93-8171-9da08de3ae6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 104,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bdf28cc7-f64f-47a5-9950-96cb1ee080e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 104,
                "offset": 4,
                "shift": 25,
                "w": 12,
                "x": 137,
                "y": 320
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a688d111-623a-41cb-aba4-8119ab68e35d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 104,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 115,
                "y": 320
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5262ab19-8813-4358-8a34-fd6920880bb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 104,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 53,
                "y": 320
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "08e84291-8d76-4e2b-9c6e-55f66b1c7d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 104,
                "offset": 3,
                "shift": 56,
                "w": 49,
                "x": 2,
                "y": 320
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d09105d9-ca37-478d-9dd0-a648060a3b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 104,
                "offset": 3,
                "shift": 80,
                "w": 75,
                "x": 939,
                "y": 214
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4d25eb68-ed00-42c7-b857-a344919a7e70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 104,
                "offset": 4,
                "shift": 67,
                "w": 60,
                "x": 877,
                "y": 214
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "04207170-c33e-43a3-abc8-035df2eef766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 104,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 866,
                "y": 214
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3ece9ddc-5373-4b09-84a4-4e21a337b924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 104,
                "offset": 4,
                "shift": 24,
                "w": 19,
                "x": 845,
                "y": 214
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b9ae9b07-409d-4cea-a063-ae9907a3e873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 104,
                "offset": 1,
                "shift": 24,
                "w": 19,
                "x": 824,
                "y": 214
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9bbc0630-80cc-4f6e-a255-f802fe5f66b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 104,
                "offset": 4,
                "shift": 36,
                "w": 29,
                "x": 151,
                "y": 320
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d19e0a1d-d5f8-4fa7-ad78-789e3a5077dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 777,
                "y": 214
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4a08722a-f9e4-4b2a-91b6-b59a65670be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 104,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 712,
                "y": 214
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "49e36c4a-dec8-4966-a61e-172e6b851368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 104,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 680,
                "y": 214
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bc8dfbf3-808b-4c5c-85cb-fec9f8d5ebd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 104,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 666,
                "y": 214
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a224b843-7c4a-4f21-a151-8accfe95f957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 626,
                "y": 214
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "08502188-41c2-4494-a3d4-b38e78e6725d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 104,
                "offset": 4,
                "shift": 53,
                "w": 45,
                "x": 579,
                "y": 214
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5726c062-646c-4ba1-a78f-152ea97cc22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 104,
                "offset": 3,
                "shift": 35,
                "w": 23,
                "x": 554,
                "y": 214
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "77552ecc-292e-4b92-847a-34732a656010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 104,
                "offset": 6,
                "shift": 54,
                "w": 44,
                "x": 508,
                "y": 214
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d93b8a47-b2ac-4af3-80dd-7ee9b25bb368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 104,
                "offset": 5,
                "shift": 55,
                "w": 46,
                "x": 460,
                "y": 214
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "52b1f11b-23b3-4171-bebc-65650fd79b62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 104,
                "offset": 1,
                "shift": 60,
                "w": 56,
                "x": 402,
                "y": 214
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5ce6af8b-215c-40b3-8aa6-cd51bab7d6ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 104,
                "offset": 6,
                "shift": 59,
                "w": 48,
                "x": 727,
                "y": 214
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "79eeb2dd-3d52-495f-a74f-9757e9f08d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 104,
                "offset": 6,
                "shift": 54,
                "w": 44,
                "x": 182,
                "y": 320
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "66180780-b405-4c2d-89d0-70df97dfa53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 104,
                "offset": 4,
                "shift": 53,
                "w": 46,
                "x": 228,
                "y": 320
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c5116bad-ede1-4c78-9ad4-94335dcfc079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 104,
                "offset": 5,
                "shift": 55,
                "w": 45,
                "x": 276,
                "y": 320
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2113c002-bcd2-4717-bb33-19b2a414547d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 104,
                "offset": 4,
                "shift": 54,
                "w": 44,
                "x": 380,
                "y": 426
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c9686e58-cc07-4b8a-95fb-c156d1ec5d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 104,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 366,
                "y": 426
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "be3db226-f720-430f-9a6b-7b279bd9f54e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 104,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 350,
                "y": 426
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "591029e4-0670-45d0-8058-b49b77fc4d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 303,
                "y": 426
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4dfccc2e-1238-4d39-a40b-19a2e888cb67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 104,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 260,
                "y": 426
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bfa98ede-05af-4978-9307-39972dffb0ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 213,
                "y": 426
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7a55eaab-6ddd-42fa-9009-8b99350d491c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 166,
                "y": 426
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "60feaf14-0e02-4945-8217-7863986c2ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 104,
                "offset": 3,
                "shift": 102,
                "w": 96,
                "x": 68,
                "y": 426
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "16e51939-1dde-4705-8838-b6ca9cc2bea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 104,
                "offset": 1,
                "shift": 66,
                "w": 64,
                "x": 2,
                "y": 426
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "460df9e5-6e17-4481-90b7-809424683c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 104,
                "offset": 9,
                "shift": 65,
                "w": 51,
                "x": 914,
                "y": 320
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "89afda74-79ed-4d65-b794-ba1721a83a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 104,
                "offset": 5,
                "shift": 72,
                "w": 62,
                "x": 850,
                "y": 320
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "13fb7618-9eb0-44c4-9892-3c4f64aa8253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 104,
                "offset": 9,
                "shift": 72,
                "w": 58,
                "x": 790,
                "y": 320
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "504e3408-61a4-4fda-8ee3-c2d040435783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 104,
                "offset": 9,
                "shift": 65,
                "w": 52,
                "x": 736,
                "y": 320
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4a87e24d-a420-469c-a217-9369c91759ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 104,
                "offset": 3,
                "shift": 58,
                "w": 52,
                "x": 682,
                "y": 320
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "230b168f-4b04-4742-827e-f5fb56fcb0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 104,
                "offset": 5,
                "shift": 72,
                "w": 62,
                "x": 618,
                "y": 320
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2393705f-ff1e-4884-a42d-09cc94b7d98b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 104,
                "offset": 9,
                "shift": 79,
                "w": 61,
                "x": 555,
                "y": 320
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ebc06318-74ed-4b70-93d7-37bf50442fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 104,
                "offset": 9,
                "shift": 26,
                "w": 8,
                "x": 545,
                "y": 320
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "036e280b-a1f5-42a9-84d6-522907b52562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 104,
                "offset": 4,
                "shift": 57,
                "w": 44,
                "x": 499,
                "y": 320
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bde1b75e-9009-477c-b32c-ad5d53f303ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 104,
                "offset": 9,
                "shift": 61,
                "w": 48,
                "x": 449,
                "y": 320
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "38238e87-976e-4383-ab83-a6ed1986f494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 104,
                "offset": 9,
                "shift": 59,
                "w": 48,
                "x": 399,
                "y": 320
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cf146183-3a20-46e4-b143-4c8f21213391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 104,
                "offset": 9,
                "shift": 92,
                "w": 74,
                "x": 323,
                "y": 320
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a4ffa02f-1cba-4609-9e06-f14dbf686318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 104,
                "offset": 9,
                "shift": 77,
                "w": 59,
                "x": 341,
                "y": 214
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0fe6d818-28dd-4f05-bfe6-386255856eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 104,
                "offset": 5,
                "shift": 86,
                "w": 76,
                "x": 263,
                "y": 214
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "65078f01-3d58-4e0f-8678-81aa24a905cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 104,
                "offset": 9,
                "shift": 57,
                "w": 47,
                "x": 214,
                "y": 214
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fdfca18b-ae46-4d2f-a27e-3ef86d3f6164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 104,
                "offset": 5,
                "shift": 86,
                "w": 76,
                "x": 130,
                "y": 108
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "70c0072a-44fc-400c-aa1f-1f06be9d4ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 104,
                "offset": 9,
                "shift": 58,
                "w": 48,
                "x": 55,
                "y": 108
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b0988fd4-a3b6-4d21-ba69-8e8ce02be85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 104,
                "offset": 5,
                "shift": 62,
                "w": 51,
                "x": 2,
                "y": 108
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fdacc93b-b985-432f-b474-4849290512a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 104,
                "offset": 1,
                "shift": 61,
                "w": 59,
                "x": 910,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3ed4233b-dd3e-41fa-90f1-07d362cb4c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 104,
                "offset": 9,
                "shift": 76,
                "w": 59,
                "x": 849,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3bb5e6a3-9496-46ec-a713-46a2cc212967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 104,
                "offset": 1,
                "shift": 67,
                "w": 65,
                "x": 782,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2fb596f0-5632-491f-9a7c-0a181f3c7451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 104,
                "offset": 1,
                "shift": 88,
                "w": 85,
                "x": 695,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "989b605b-f886-429e-b10d-5f5854c2495e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 104,
                "offset": 4,
                "shift": 66,
                "w": 58,
                "x": 635,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d936cc3f-82e4-4a28-9bfc-72d4065dd7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 104,
                "offset": 0,
                "shift": 60,
                "w": 61,
                "x": 572,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "45512cd4-c37b-4274-9820-1363c72f6a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 104,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 510,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c6fc3927-d5e7-45cd-b9e7-804e1aaef6a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 104,
                "offset": 6,
                "shift": 28,
                "w": 23,
                "x": 105,
                "y": 108
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "848bdc2d-215f-45f1-83b3-5886e0500dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "80092381-53c1-4938-b819-5b596e6f756c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 104,
                "offset": 0,
                "shift": 28,
                "w": 23,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "48416c18-1810-4e65-8f6d-869a14543bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 370,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d2a8a16b-3861-46dd-8a5e-618590240a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 104,
                "offset": -4,
                "shift": 44,
                "w": 53,
                "x": 315,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "31debc14-ae54-4549-8957-39e2095d06c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 104,
                "offset": 4,
                "shift": 25,
                "w": 18,
                "x": 295,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4f0492c5-8610-4054-9718-645b892bce14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 104,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "45d9e97e-3c57-406e-9eed-558e24272fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 104,
                "offset": 8,
                "shift": 66,
                "w": 53,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "59a0bbdc-a424-413d-9344-f1af8e724427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 104,
                "offset": 5,
                "shift": 56,
                "w": 46,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d877df09-6420-4659-9d7f-27fbb6e19817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 104,
                "offset": 5,
                "shift": 66,
                "w": 53,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "19ec8d92-4ac1-4315-bd0a-ad509d613418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 104,
                "offset": 5,
                "shift": 57,
                "w": 49,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eb77e6b6-079a-4220-9174-0c139e7e11aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 104,
                "offset": 2,
                "shift": 34,
                "w": 33,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e2d01de8-1d12-4c0a-a624-009b7cc626c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 104,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 208,
                "y": 108
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b29b2f18-5ff1-41a3-b815-fbe45d8d9f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 104,
                "offset": 8,
                "shift": 63,
                "w": 48,
                "x": 667,
                "y": 108
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f43c3656-1f21-4c8e-b9ce-07321f140e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 104,
                "offset": 8,
                "shift": 27,
                "w": 11,
                "x": 263,
                "y": 108
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b2278191-42b7-4a6c-b890-d9ac75414ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 104,
                "offset": -9,
                "shift": 27,
                "w": 28,
                "x": 140,
                "y": 214
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2023b992-398e-48c7-a9df-8b93a9c12b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 104,
                "offset": 8,
                "shift": 53,
                "w": 40,
                "x": 98,
                "y": 214
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2492a494-22e6-4cce-adf1-3a4b59c26d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 104,
                "offset": 6,
                "shift": 26,
                "w": 19,
                "x": 77,
                "y": 214
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "551b6ee8-3d82-4006-934f-1aa21025aec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 104,
                "offset": 8,
                "shift": 88,
                "w": 73,
                "x": 2,
                "y": 214
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1630f061-40a5-4f92-940c-9070a064b889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 104,
                "offset": 8,
                "shift": 63,
                "w": 48,
                "x": 953,
                "y": 108
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3af492ff-34ac-46de-8b22-aff4f0c8e2ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 104,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 898,
                "y": 108
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8946fd64-18f5-4b70-9808-edf8d5192d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 104,
                "offset": 8,
                "shift": 66,
                "w": 53,
                "x": 843,
                "y": 108
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5bc24caa-2596-4cd3-aa72-52ede2c7f01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 104,
                "offset": 5,
                "shift": 66,
                "w": 53,
                "x": 788,
                "y": 108
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "cf5b09e9-3762-4b88-b0f7-f689c01f5d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 104,
                "offset": 8,
                "shift": 43,
                "w": 33,
                "x": 753,
                "y": 108
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0121e246-f742-434a-a14c-106cadcb96bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 104,
                "offset": 5,
                "shift": 50,
                "w": 42,
                "x": 170,
                "y": 214
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "727e6a68-0741-4177-b216-f259a6f93d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 104,
                "offset": 1,
                "shift": 38,
                "w": 34,
                "x": 717,
                "y": 108
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b992be16-1ff4-430e-8f20-0c7fadcdaea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 104,
                "offset": 7,
                "shift": 63,
                "w": 48,
                "x": 617,
                "y": 108
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2f78d0ee-377f-47e5-9399-b4a1b068de3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 104,
                "offset": 1,
                "shift": 52,
                "w": 50,
                "x": 565,
                "y": 108
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "12b517a1-5d83-4d8e-9bab-ddee1100cf6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 104,
                "offset": 1,
                "shift": 69,
                "w": 67,
                "x": 496,
                "y": 108
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bdeb28cf-b614-40e0-be84-fc3a1b884186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 449,
                "y": 108
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "50be6603-2835-40fb-84fd-288368adf70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 104,
                "offset": 1,
                "shift": 51,
                "w": 51,
                "x": 396,
                "y": 108
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "124fdfac-393e-4f88-b3e9-9fec7013c964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 104,
                "offset": 5,
                "shift": 51,
                "w": 43,
                "x": 351,
                "y": 108
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5a081b0d-d23d-46b6-800e-5da36db2e9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 104,
                "offset": 4,
                "shift": 35,
                "w": 31,
                "x": 318,
                "y": 108
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "efd97e76-b0dc-4f33-95f5-41b7feec3778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 104,
                "offset": 14,
                "shift": 36,
                "w": 8,
                "x": 308,
                "y": 108
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5402c812-03ab-4f35-a163-6b2646846487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 104,
                "offset": 1,
                "shift": 35,
                "w": 30,
                "x": 276,
                "y": 108
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9bff49f3-0397-4c7d-9545-827260f3fc81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 39,
                "x": 426,
                "y": 426
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ae68441e-ebe9-42ff-9f6f-bceb4f26ac62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 104,
                "offset": 18,
                "shift": 90,
                "w": 54,
                "x": 467,
                "y": 426
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 70,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}