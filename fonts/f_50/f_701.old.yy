{
    "id": "cf4a3326-9341-49d7-9ddd-ce10d6a3eabf",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_70",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_70\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "55d50a5d-da7a-4566-adc5-f5375b35cf17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 104,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9d5792dc-85bb-4b59-a1c4-b7b17be4ca8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 104,
                "offset": 4,
                "shift": 25,
                "w": 12,
                "x": 137,
                "y": 320
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "694daed6-d7d3-455c-a968-c8a4740a93e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 104,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 115,
                "y": 320
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2e3ae9e0-cddc-4e12-96fa-c71fd0007b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 104,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 53,
                "y": 320
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9f447f51-e47d-487d-844e-527e690714b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 104,
                "offset": 3,
                "shift": 56,
                "w": 49,
                "x": 2,
                "y": 320
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "97f6259e-a990-4e66-aac7-5eca57ebb178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 104,
                "offset": 3,
                "shift": 80,
                "w": 75,
                "x": 939,
                "y": 214
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3f3da73c-ac5f-477b-a98d-b5f5c58c4da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 104,
                "offset": 4,
                "shift": 67,
                "w": 60,
                "x": 877,
                "y": 214
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9cb6b319-460f-41c7-a7ad-09a1703d91a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 104,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 866,
                "y": 214
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2cdaf7ad-0ef2-47fa-b1a2-60d2d7452416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 104,
                "offset": 4,
                "shift": 24,
                "w": 19,
                "x": 845,
                "y": 214
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cad50e4f-254d-44b8-ac46-96d15295b200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 104,
                "offset": 1,
                "shift": 24,
                "w": 19,
                "x": 824,
                "y": 214
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "56c5dc37-b023-4fef-9a9b-7797d7bcc82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 104,
                "offset": 4,
                "shift": 36,
                "w": 29,
                "x": 151,
                "y": 320
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6f64ee3a-66d3-43d8-846e-65d444484c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 777,
                "y": 214
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5aef5af6-36a8-4656-b356-eac6899af047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 104,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 712,
                "y": 214
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "819b62e3-7a06-477c-81a4-acdb16a08775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 104,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 680,
                "y": 214
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2b9a15c2-3d8f-4c06-a6ee-acdc59efd8aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 104,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 666,
                "y": 214
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7eab0221-9a3d-46ef-8929-cdd747cab778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 626,
                "y": 214
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1c3e7543-819f-4a70-ad3f-7a1171a503e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 104,
                "offset": 4,
                "shift": 53,
                "w": 45,
                "x": 579,
                "y": 214
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "857f9d02-0932-4e14-9895-273bdf19a370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 104,
                "offset": 3,
                "shift": 35,
                "w": 23,
                "x": 554,
                "y": 214
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "949b1af3-044b-45e7-b8d5-b8041f0412fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 104,
                "offset": 6,
                "shift": 54,
                "w": 44,
                "x": 508,
                "y": 214
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "de59b9e7-5ced-40fe-a021-35ee0f8ccfcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 104,
                "offset": 5,
                "shift": 55,
                "w": 46,
                "x": 460,
                "y": 214
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3b179570-ad1a-4dfc-84b2-14d3ca6a39a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 104,
                "offset": 1,
                "shift": 60,
                "w": 56,
                "x": 402,
                "y": 214
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0d6d91a4-41b8-4c38-a315-35d284306246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 104,
                "offset": 6,
                "shift": 59,
                "w": 48,
                "x": 727,
                "y": 214
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3db3fca5-7fa4-4b36-ad54-d37c805608be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 104,
                "offset": 6,
                "shift": 54,
                "w": 44,
                "x": 182,
                "y": 320
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c112e57b-444d-423c-be37-f7eb2080b7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 104,
                "offset": 4,
                "shift": 53,
                "w": 46,
                "x": 228,
                "y": 320
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fd72c422-e2ac-4b3f-bf05-7b5f6df7511f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 104,
                "offset": 5,
                "shift": 55,
                "w": 45,
                "x": 276,
                "y": 320
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "48f5725e-38f9-4a14-ba7c-6f207c660bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 104,
                "offset": 4,
                "shift": 54,
                "w": 44,
                "x": 380,
                "y": 426
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6bc3e072-add9-457d-b1df-7934b9512a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 104,
                "offset": 2,
                "shift": 18,
                "w": 12,
                "x": 366,
                "y": 426
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a76d8303-de0b-4156-83dd-f50d25f026a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 104,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 350,
                "y": 426
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a0c33586-f6f5-40c6-8d6a-303d82c55af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 303,
                "y": 426
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7a03faf5-572b-42ba-81a9-f13297d4f973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 104,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 260,
                "y": 426
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f69be567-903a-4048-956a-0de1dbc03410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 213,
                "y": 426
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a065c2b8-dea6-4d82-a9e5-3c8f9e30b9a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 166,
                "y": 426
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ca1faef6-d3e5-4be3-9a4d-b5d0d7963ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 104,
                "offset": 3,
                "shift": 102,
                "w": 96,
                "x": 68,
                "y": 426
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f2448b22-77af-4864-b148-f73da63c0aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 104,
                "offset": 1,
                "shift": 66,
                "w": 64,
                "x": 2,
                "y": 426
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f853185f-08f2-4d03-ac1b-ee63f008daee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 104,
                "offset": 9,
                "shift": 65,
                "w": 51,
                "x": 914,
                "y": 320
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0b0edd87-ed91-4a47-9b88-5f01636d6656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 104,
                "offset": 5,
                "shift": 72,
                "w": 62,
                "x": 850,
                "y": 320
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "91d685a3-a6b4-47dc-bfa0-eae9fb127976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 104,
                "offset": 9,
                "shift": 72,
                "w": 58,
                "x": 790,
                "y": 320
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "51c4378e-e9cb-4931-9fe5-dd65888b4249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 104,
                "offset": 9,
                "shift": 65,
                "w": 52,
                "x": 736,
                "y": 320
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b962792b-f2cd-48d1-9e0d-b455b2c59972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 104,
                "offset": 3,
                "shift": 58,
                "w": 52,
                "x": 682,
                "y": 320
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e23e08c8-a943-4c2a-9443-27f235d35928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 104,
                "offset": 5,
                "shift": 72,
                "w": 62,
                "x": 618,
                "y": 320
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d90ebbad-fee4-45a4-8db1-85e95b5f7cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 104,
                "offset": 9,
                "shift": 79,
                "w": 61,
                "x": 555,
                "y": 320
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "89ba350f-6e12-4d5b-90b2-4b7c852cfc9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 104,
                "offset": 9,
                "shift": 26,
                "w": 8,
                "x": 545,
                "y": 320
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "fb154eb4-cf38-4813-bb42-5e76095373b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 104,
                "offset": 4,
                "shift": 57,
                "w": 44,
                "x": 499,
                "y": 320
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bc6cbd5b-426f-42cd-ac86-967d571f02a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 104,
                "offset": 9,
                "shift": 61,
                "w": 48,
                "x": 449,
                "y": 320
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c743e719-09e2-46f1-a98f-fe0bdf65b4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 104,
                "offset": 9,
                "shift": 59,
                "w": 48,
                "x": 399,
                "y": 320
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "21bea28f-4699-4a75-bf5d-94f64ad49aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 104,
                "offset": 9,
                "shift": 92,
                "w": 74,
                "x": 323,
                "y": 320
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3ba31a8c-366a-4f13-95f7-3f6d71ec48e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 104,
                "offset": 9,
                "shift": 77,
                "w": 59,
                "x": 341,
                "y": 214
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5c7885f5-e188-4808-b93f-5e539518d2d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 104,
                "offset": 5,
                "shift": 86,
                "w": 76,
                "x": 263,
                "y": 214
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "71b661ff-c08d-4133-b050-83f882a18416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 104,
                "offset": 9,
                "shift": 57,
                "w": 47,
                "x": 214,
                "y": 214
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ce0c886c-a011-4b29-a01f-485af00da9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 104,
                "offset": 5,
                "shift": 86,
                "w": 76,
                "x": 130,
                "y": 108
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "853812cc-d3a1-44ee-99b5-b182b2fa95f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 104,
                "offset": 9,
                "shift": 58,
                "w": 48,
                "x": 55,
                "y": 108
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0d1adabf-b893-4135-8bb1-697cfb9b5dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 104,
                "offset": 5,
                "shift": 62,
                "w": 51,
                "x": 2,
                "y": 108
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9bfaf96f-c5f0-4bed-b3c4-23521dadc315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 104,
                "offset": 1,
                "shift": 61,
                "w": 59,
                "x": 910,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "df929bbe-9239-4ea1-9936-e3bef176e7f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 104,
                "offset": 9,
                "shift": 76,
                "w": 59,
                "x": 849,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c6a7bba8-ab0b-40a4-9bb7-1c788889e2a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 104,
                "offset": 1,
                "shift": 67,
                "w": 65,
                "x": 782,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7ee6997e-a746-4392-84fa-d789832fe673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 104,
                "offset": 1,
                "shift": 88,
                "w": 85,
                "x": 695,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "663ed5ae-4166-43de-8216-61fb976ee1ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 104,
                "offset": 4,
                "shift": 66,
                "w": 58,
                "x": 635,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "eab7576c-e64c-4324-8232-409bbd40716c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 104,
                "offset": 0,
                "shift": 60,
                "w": 61,
                "x": 572,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "15a2d71c-0e25-411b-b648-8c9b04037b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 104,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 510,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b81ccef0-f62f-4a4a-8134-653b4e157dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 104,
                "offset": 6,
                "shift": 28,
                "w": 23,
                "x": 105,
                "y": 108
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3452a536-64ef-44fe-9ad0-a7c2d92155c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8679b2c1-b50b-4aa3-9778-4d4bd1021c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 104,
                "offset": 0,
                "shift": 28,
                "w": 23,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e0b0297f-6b82-492a-8f6b-b55be1f43e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 370,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "219887e2-f453-4919-8b19-c7578c066857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 104,
                "offset": -4,
                "shift": 44,
                "w": 53,
                "x": 315,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "35dcb97a-89d1-487d-b8fb-4c3e2d10ca82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 104,
                "offset": 4,
                "shift": 25,
                "w": 18,
                "x": 295,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d4100fc3-5546-4231-af06-ec623601de0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 104,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2e4aa2d5-74a7-4d41-aa0c-22d3850341a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 104,
                "offset": 8,
                "shift": 66,
                "w": 53,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6ebd9649-5553-4b71-bb07-977bb3f1f16d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 104,
                "offset": 5,
                "shift": 56,
                "w": 46,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ee43c7b2-587c-466c-bda0-f5f316f7f944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 104,
                "offset": 5,
                "shift": 66,
                "w": 53,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8a14f910-d28c-46df-8220-5d5674d11555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 104,
                "offset": 5,
                "shift": 57,
                "w": 49,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "aa9b1747-ac73-428d-8f29-7f966a392805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 104,
                "offset": 2,
                "shift": 34,
                "w": 33,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "20585883-3bd7-45f9-8a05-4a8a15b8be81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 104,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 208,
                "y": 108
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "85d92775-a8a6-4602-b472-468527797dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 104,
                "offset": 8,
                "shift": 63,
                "w": 48,
                "x": 667,
                "y": 108
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6ef7d5b3-83fd-4945-b4e4-a1104f4a2596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 104,
                "offset": 8,
                "shift": 27,
                "w": 11,
                "x": 263,
                "y": 108
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d28cac57-a3fa-4d1a-acb5-848826b384f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 104,
                "offset": -9,
                "shift": 27,
                "w": 28,
                "x": 140,
                "y": 214
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "db607a51-0331-46a5-b653-2c862adc3f86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 104,
                "offset": 8,
                "shift": 53,
                "w": 40,
                "x": 98,
                "y": 214
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d0c03169-8de0-4ab4-a0f4-32bccb25fa29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 104,
                "offset": 6,
                "shift": 26,
                "w": 19,
                "x": 77,
                "y": 214
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5951744d-0868-4986-abaf-ae8af848d273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 104,
                "offset": 8,
                "shift": 88,
                "w": 73,
                "x": 2,
                "y": 214
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "147077e3-0539-409d-8ba9-2e68dcca0b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 104,
                "offset": 8,
                "shift": 63,
                "w": 48,
                "x": 953,
                "y": 108
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "32137eaa-ac96-4dd0-a58d-feb1ec38be59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 104,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 898,
                "y": 108
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c7e79788-275b-4ef0-ab1f-6b636e3b8c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 104,
                "offset": 8,
                "shift": 66,
                "w": 53,
                "x": 843,
                "y": 108
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d616a4c6-f089-4bbf-9e88-9f9141c29a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 104,
                "offset": 5,
                "shift": 66,
                "w": 53,
                "x": 788,
                "y": 108
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f51e631c-ec58-45a7-91df-6ea36f567e04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 104,
                "offset": 8,
                "shift": 43,
                "w": 33,
                "x": 753,
                "y": 108
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2738285c-866a-4435-9ada-39f770121f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 104,
                "offset": 5,
                "shift": 50,
                "w": 42,
                "x": 170,
                "y": 214
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3cd596ec-b353-4302-9f17-a34d8d8f1cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 104,
                "offset": 1,
                "shift": 38,
                "w": 34,
                "x": 717,
                "y": 108
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8f927c97-3421-45e2-b269-6a139bc1e62d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 104,
                "offset": 7,
                "shift": 63,
                "w": 48,
                "x": 617,
                "y": 108
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ec966eca-6c30-4aae-aad3-1bc43a63ac4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 104,
                "offset": 1,
                "shift": 52,
                "w": 50,
                "x": 565,
                "y": 108
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2c23d616-86e9-489a-a907-c46b3184bafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 104,
                "offset": 1,
                "shift": 69,
                "w": 67,
                "x": 496,
                "y": 108
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d7e49ff8-e891-45a7-800e-bb05628de72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 104,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 449,
                "y": 108
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "136207ce-99aa-43b4-8fdc-da81495f337e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 104,
                "offset": 1,
                "shift": 51,
                "w": 51,
                "x": 396,
                "y": 108
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b6c0809a-31bb-4858-9619-3a8b92721d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 104,
                "offset": 5,
                "shift": 51,
                "w": 43,
                "x": 351,
                "y": 108
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "67499ebe-62e3-4f3d-a4fa-d4bd7818068f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 104,
                "offset": 4,
                "shift": 35,
                "w": 31,
                "x": 318,
                "y": 108
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "10db2161-8c5f-4074-82f7-5ca28285c383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 104,
                "offset": 14,
                "shift": 36,
                "w": 8,
                "x": 308,
                "y": 108
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "553f7996-640a-43a6-972f-b65232deb4af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 104,
                "offset": 1,
                "shift": 35,
                "w": 30,
                "x": 276,
                "y": 108
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "48ebe5ff-da82-4f8c-af5b-e0b811558346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 104,
                "offset": 3,
                "shift": 44,
                "w": 39,
                "x": 426,
                "y": 426
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "d178185b-ecec-49ec-8226-7e2495b3c23c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 104,
                "offset": 18,
                "shift": 90,
                "w": 54,
                "x": 467,
                "y": 426
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 70,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}