{
    "id": "270fcb8f-c217-48eb-bdb2-6f3c2b6022d3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_401",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\f_401\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cf8b41a5-59d5-4531-a04c-2af22f270082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 60,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "254a3cf8-a99a-46c1-9980-01da056c4790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 60,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 363,
                "y": 188
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1b609127-f89f-4e8c-961f-65e1c8a60bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 60,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 350,
                "y": 188
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ad4046d4-78d3-4a12-a77e-b2dc04a96dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 60,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 313,
                "y": 188
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "036096ce-7496-4cf7-8965-d616613d2889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 60,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 283,
                "y": 188
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "991eb6be-700b-4258-b6c1-bd54f5d70540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 60,
                "offset": 1,
                "shift": 46,
                "w": 44,
                "x": 237,
                "y": 188
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "70778375-e37e-452a-88b6-2c38d8f29526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 60,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 200,
                "y": 188
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4246165e-972b-4515-bc80-4caae03fd898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 60,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 193,
                "y": 188
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "77c440d2-cef9-442f-852a-9348b48d3a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 60,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 179,
                "y": 188
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fdefb168-dfe9-4123-a4ee-2aab91094afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 60,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 166,
                "y": 188
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "92b5d512-2cf5-4a25-9e70-088b832ad6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 372,
                "y": 188
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d9fe29e7-7494-4f5b-9b53-b8c1c3f641e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 138,
                "y": 188
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0c66d211-760d-4a48-82a9-9e3ac10f68de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 60,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 98,
                "y": 188
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "955c644f-4d0e-4e2c-a8de-1f59ee19e3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 60,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 79,
                "y": 188
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "93f5d9da-7fd6-432d-83ec-702d1409fb55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 60,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 70,
                "y": 188
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "68fafe3b-caaf-41dd-bede-4d5de3c0a6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 45,
                "y": 188
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "41dd57b7-f685-4d56-9145-ce2e2bd7c59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 60,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 17,
                "y": 188
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4e23e151-492a-4a81-9fcc-285bf67c6bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 13,
                "x": 2,
                "y": 188
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0d8974f5-8496-4c6a-a9ae-6ccaad40d50f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 476,
                "y": 126
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "16eae2b6-2132-4f39-b290-819f9aeb2155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 60,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 447,
                "y": 126
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "45123653-4d68-44e5-9cba-3ab53c69d4d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 412,
                "y": 126
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "09e12b17-0fd0-4fcc-baff-899da9782ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 60,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 108,
                "y": 188
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e1583784-e71f-48eb-b1f9-41d8fa65fbb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 391,
                "y": 188
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f173aa82-7272-4f29-b891-7cc12a1b47d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 60,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 419,
                "y": 188
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cfe6813e-99a8-49c3-9434-7491541938cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 60,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 448,
                "y": 188
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "05d34bf3-e41a-41c5-8f70-4c006b5e3ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 60,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 103,
                "y": 312
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1c39c83f-fdd7-4d93-a3d1-1387d0e86dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 60,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 94,
                "y": 312
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ff255e3a-8ace-43e6-beb3-b647e9171d3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 60,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 83,
                "y": 312
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9aaacbfd-d5ce-446f-9343-75de46e9201c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 55,
                "y": 312
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bb37779b-e1fe-4265-aa51-e01d987c6cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 60,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 30,
                "y": 312
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1ab86c81-4ff3-47df-9402-452514706a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 2,
                "y": 312
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "276b679f-3c94-4fb8-b3b9-1e66a2724609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 480,
                "y": 250
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "54a50900-b81c-4def-acf1-ecf809fbca5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 60,
                "offset": 2,
                "shift": 58,
                "w": 55,
                "x": 423,
                "y": 250
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "311fd20a-8d9c-456e-b940-df9967469310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 60,
                "offset": 0,
                "shift": 37,
                "w": 38,
                "x": 383,
                "y": 250
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5c7271b0-b273-4af5-bcbb-6d889fa19ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 60,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 351,
                "y": 250
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fc9cde12-b917-4634-8de2-346e981e1cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 60,
                "offset": 3,
                "shift": 41,
                "w": 35,
                "x": 314,
                "y": 250
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "315a0602-4763-4fc4-b3b8-494634404293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 60,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 279,
                "y": 250
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9fa5f065-c67b-4f4d-ae71-a9f3aca89d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 60,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 247,
                "y": 250
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1aa85b57-a089-4941-8e26-09b1c049a830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 60,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 215,
                "y": 250
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1355cd89-18e0-49fd-b9fa-0f92e2576680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 60,
                "offset": 3,
                "shift": 41,
                "w": 35,
                "x": 178,
                "y": 250
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d78afc2f-d4da-489a-8aa6-ce9913ebc99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 60,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 141,
                "y": 250
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "57dc1fbd-5abe-442d-9023-127c846b46c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 60,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 134,
                "y": 250
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "26f99a5c-3772-4a50-ade0-802968ce5272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 60,
                "offset": 2,
                "shift": 33,
                "w": 26,
                "x": 106,
                "y": 250
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3759859d-c677-4712-9678-6e85e4b927c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 60,
                "offset": 5,
                "shift": 35,
                "w": 28,
                "x": 76,
                "y": 250
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7598e7c1-7c7e-40ac-a09a-aac1019f6e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 60,
                "offset": 5,
                "shift": 34,
                "w": 28,
                "x": 46,
                "y": 250
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b7e5005a-3883-4d00-8d57-577ab63a06b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 60,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 2,
                "y": 250
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4306499b-7c41-47d5-8e73-e0d91af15c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 60,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 376,
                "y": 126
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b523a75c-ae10-4b9d-9fa0-4e1d0de0364e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 60,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 331,
                "y": 126
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "145817a1-73bb-49cd-aa7d-2fe907463a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 60,
                "offset": 5,
                "shift": 33,
                "w": 27,
                "x": 302,
                "y": 126
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "52b93c3e-deb5-41c2-8b2d-214b745180c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 60,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 152,
                "y": 64
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5de63757-185b-435f-837b-3fcd81fc26be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 60,
                "offset": 5,
                "shift": 33,
                "w": 28,
                "x": 106,
                "y": 64
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "625c7a10-4678-4b13-adef-f93ce63a7f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 60,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 75,
                "y": 64
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "62598168-0197-4dc7-a9fd-bf3315749765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 38,
                "y": 64
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8494d3a9-0d7c-4377-a671-5a38ad8f9dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 60,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cb5a50b9-8daf-4991-8b64-862adaf153fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 60,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "99367df6-a00a-47c2-80ac-4f9581d7262f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 60,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5210643f-360c-401b-8b9b-846f471309fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 60,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "14cabd3e-e3b1-46da-bb2a-d4bb61b08246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bcfb3ad8-04af-4bd1-9fa0-a268726266fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 60,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0fe2a6b3-011a-432a-a8fe-843ec5ba9776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 60,
                "offset": 3,
                "shift": 16,
                "w": 14,
                "x": 136,
                "y": 64
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "eb84d159-a76c-4e9f-b85b-138de6301c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 60,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b2b8b227-09fd-46dd-b63f-a08e4cbcd51e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 60,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ee7c0687-a712-431c-b880-277b0bca4c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 60,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "19051eba-bebc-4880-b546-5bc925415cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 60,
                "offset": -3,
                "shift": 25,
                "w": 31,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b7340639-97cd-4e55-9406-06aceae1aac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 60,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "40090bad-9838-47f6-83ba-7ad7a9f48cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4fbb5b02-4b0d-49ea-a856-07acdaf4e11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 60,
                "offset": 4,
                "shift": 38,
                "w": 31,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b5c8b63d-2d2a-4dde-8cf5-f38cf3460642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 60,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "93041f17-f4a9-4fd4-a9bd-bef019cc8e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 60,
                "offset": 3,
                "shift": 38,
                "w": 30,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "da544885-46da-4703-8ec8-3159cebcdf56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 60,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b4ed2389-117c-4036-bd2b-eaac342ff108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 60,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 260,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fe02fd6a-4d8a-4ff8-80e8-fa34f2206104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 197,
                "y": 64
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ffec852e-2f4e-4ce6-8288-e7011e06ad99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 473,
                "y": 64
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0110f1ed-b208-4541-82b8-9d7e6d84cf71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 60,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 229,
                "y": 64
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6e4a663a-0464-4935-95ff-20d8d84051bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 60,
                "offset": -6,
                "shift": 15,
                "w": 17,
                "x": 257,
                "y": 126
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c8cd400d-9bb7-4221-90e6-6d5764172d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 60,
                "offset": 4,
                "shift": 31,
                "w": 24,
                "x": 231,
                "y": 126
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "092a8890-3b4c-4a8a-a1f3-fba3ec5ea28f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 60,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 217,
                "y": 126
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d053dd86-debb-4bcc-b74d-46209483123f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 60,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 172,
                "y": 126
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a9569fa9-c161-4b94-bc87-757d2b14cca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 142,
                "y": 126
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "60beaa72-85cc-4a0b-bacf-d12803869e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 110,
                "y": 126
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1b8a3604-2fcc-485f-bd57-2ca2fdb9c6f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 60,
                "offset": 4,
                "shift": 38,
                "w": 31,
                "x": 77,
                "y": 126
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "14cf78ab-d015-41d6-b8d7-32ed9f88e1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 60,
                "offset": 3,
                "shift": 38,
                "w": 30,
                "x": 45,
                "y": 126
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "91f01341-14b8-4b18-8c3f-f45373231a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 60,
                "offset": 4,
                "shift": 24,
                "w": 20,
                "x": 23,
                "y": 126
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3927d80c-7250-4b3a-b4e6-3942be2549bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 60,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 276,
                "y": 126
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8e9e2d41-2f67-4b04-ac67-b3862cb5de86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "28968f8d-c173-4019-bedc-10a6912bd15c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 443,
                "y": 64
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4d79d324-da35-4e88-b529-1501861927d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 60,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 412,
                "y": 64
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7f9f56a4-ea16-4480-8ee8-deef4ebe61a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 60,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 371,
                "y": 64
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a8c1032a-ff9f-4de6-b782-0803b73d5b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 343,
                "y": 64
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a6431e0a-a13b-451a-a757-4e5c7eeb96f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 60,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 311,
                "y": 64
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0ab3074f-cd25-428c-ab89-8f0d7cb37e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 60,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 285,
                "y": 64
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5883801b-f8b2-4091-bbe1-84bca40b3c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 265,
                "y": 64
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "476d8f34-7b74-4646-9b6c-adb25c2a99b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 60,
                "offset": 8,
                "shift": 21,
                "w": 5,
                "x": 258,
                "y": 64
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6765e13e-1843-42d5-8c89-a95d15e89052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 60,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 238,
                "y": 64
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "02e7a533-2c92-449a-8896-efcae9b613f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 131,
                "y": 312
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c3198aee-60ca-4b5c-9379-8c0d93904e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 60,
                "offset": 10,
                "shift": 51,
                "w": 31,
                "x": 156,
                "y": 312
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}