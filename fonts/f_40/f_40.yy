{
    "id": "37734952-d812-419f-9a1d-04f411784851",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_40",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_40\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "21aca3ab-ce45-4cb1-bb28-f3da9eaee0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 60,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ee13b308-4196-4c72-8c35-c6cc1f5c5f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 60,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 363,
                "y": 188
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d113f573-129b-4bb7-9a90-1fcaa6631c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 60,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 350,
                "y": 188
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c2c66b25-7fcd-480b-9770-361c16e86acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 60,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 313,
                "y": 188
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "34b48428-8e3c-4061-a9d2-8e26e708ce2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 60,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 283,
                "y": 188
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "41cf2f88-07ac-4b90-88fd-1fdaf6bdef70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 60,
                "offset": 1,
                "shift": 46,
                "w": 44,
                "x": 237,
                "y": 188
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "021fdb1e-16b1-45c1-a6b6-5914c4831dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 60,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 200,
                "y": 188
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4309a49c-3d15-4a69-8538-aa632fecb01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 60,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 193,
                "y": 188
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6bb98f28-d0db-4ac6-b5d3-9b7c2f445b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 60,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 179,
                "y": 188
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8cc381fe-b9a8-4e6b-82c3-124a12c2d69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 60,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 166,
                "y": 188
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d2c13bb4-ace4-4701-90b6-7194b79f294d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 372,
                "y": 188
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5778f6eb-fa9b-4222-9f24-c604eeff641a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 138,
                "y": 188
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "38a2791f-c822-4009-861c-2d22a25f81d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 60,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 98,
                "y": 188
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c0fc86a8-3736-4e6b-9edb-76cd8a92e367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 60,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 79,
                "y": 188
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1191ef5c-368a-4562-860d-9e26d95df5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 60,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 70,
                "y": 188
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e8c1c58d-786f-4f02-b46f-52cc637ca868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 45,
                "y": 188
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0b77f403-12fa-4ae4-b06a-ad7891ec6eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 60,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 17,
                "y": 188
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5b107ce7-f65f-45c9-a4a5-879285081468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 13,
                "x": 2,
                "y": 188
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "27fa5b86-79f8-4cbb-b287-7b903e93a879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 476,
                "y": 126
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "55522be5-acd8-4cb6-a943-120e4600796d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 60,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 447,
                "y": 126
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2598fc80-f4aa-4ba2-86e5-5c18f86c6681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 412,
                "y": 126
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "66f0a1ec-c3bb-4965-9783-cad24020abd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 60,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 108,
                "y": 188
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "121bb6a7-73ac-4001-9f57-d5591643a8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 60,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 391,
                "y": 188
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b8b8b725-bdc8-483c-8f8b-df5ba927f6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 60,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 419,
                "y": 188
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3dabcc65-a182-44db-b0e4-1f2a7b494989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 60,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 448,
                "y": 188
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f133ce74-679f-4f10-8ef7-39a26f79263d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 60,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 103,
                "y": 312
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "46e5ee24-e710-4b45-85da-4f75b9937364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 60,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 94,
                "y": 312
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "98bc3c7c-8270-4db0-bf85-aa8c339871e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 60,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 83,
                "y": 312
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "14dfe04f-150b-40ee-ae65-f0ca61a2ce24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 55,
                "y": 312
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1f63d067-21ac-49b7-a0fb-e202e7d9a899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 60,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 30,
                "y": 312
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2e0bfa09-0d40-4438-ab8f-3bfe7601cdb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 2,
                "y": 312
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "42bb03c7-dae1-417a-8e32-426497ace3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 480,
                "y": 250
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4a663144-f36d-4719-aa0e-df5a5f1ff44d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 60,
                "offset": 2,
                "shift": 58,
                "w": 55,
                "x": 423,
                "y": 250
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b9e8a4e8-0876-4ab2-b715-9e5618146389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 60,
                "offset": 0,
                "shift": 37,
                "w": 38,
                "x": 383,
                "y": 250
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "771418d7-2371-424f-a243-1de842fbfa3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 60,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 351,
                "y": 250
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "81cab80a-7c57-4f34-aa9c-971f2072b5c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 60,
                "offset": 3,
                "shift": 41,
                "w": 35,
                "x": 314,
                "y": 250
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4cc39778-68e1-4461-adf7-b1e1a7947f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 60,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 279,
                "y": 250
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5b294797-551b-451f-afbe-b176066d5bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 60,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 247,
                "y": 250
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "92d9af18-00f6-444f-97e2-98370927b02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 60,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 215,
                "y": 250
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6c446aca-c354-4896-9234-0e8a63a7a194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 60,
                "offset": 3,
                "shift": 41,
                "w": 35,
                "x": 178,
                "y": 250
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a78cdabb-1fb7-4788-8de5-1c0754a52574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 60,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 141,
                "y": 250
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5b421d72-635c-4d45-b906-e2bf18382123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 60,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 134,
                "y": 250
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "94a3dab2-7e46-487d-b56f-cada310c704e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 60,
                "offset": 2,
                "shift": 33,
                "w": 26,
                "x": 106,
                "y": 250
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fc19382b-56d2-43a2-b2ba-13389c5a53b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 60,
                "offset": 5,
                "shift": 35,
                "w": 28,
                "x": 76,
                "y": 250
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b588ba2c-477b-4623-8f47-1367ee906c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 60,
                "offset": 5,
                "shift": 34,
                "w": 28,
                "x": 46,
                "y": 250
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1191cb6e-5482-4f97-a034-34b495017c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 60,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 2,
                "y": 250
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5fdd4306-9977-422f-8eea-13d1bbc508a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 60,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 376,
                "y": 126
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "48c0a98e-d422-47cb-85b5-ec6cd927091b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 60,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 331,
                "y": 126
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "34f03163-e8d0-4c3c-994f-ff6999cfadaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 60,
                "offset": 5,
                "shift": 33,
                "w": 27,
                "x": 302,
                "y": 126
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6a1a9fe4-a741-4f47-8ca1-4a6ef3ef1ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 60,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 152,
                "y": 64
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "34ff033c-ec52-4c63-b493-000c9a14d02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 60,
                "offset": 5,
                "shift": 33,
                "w": 28,
                "x": 106,
                "y": 64
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d1367fb6-a214-4096-8038-503f6fe2b53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 60,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 75,
                "y": 64
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "778a4c6a-f215-4e09-b6e0-65493c4987ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 38,
                "y": 64
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1f70eb24-746f-461b-8ba1-7d407e50f4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 60,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0f71a6d3-5824-4243-a914-2f9dbbde153a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 60,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5027d5df-eb8f-4366-9ecc-56633aa53be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 60,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "72e0e0ba-2cd2-42db-8539-e983260b5b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 60,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bb3632ef-7081-426e-9543-b547bf782f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 60,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bcc5a51a-ddf1-4a54-9760-ca49648b8b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 60,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "069a8215-d729-4358-b18e-ab8e657c8a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 60,
                "offset": 3,
                "shift": 16,
                "w": 14,
                "x": 136,
                "y": 64
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e2e639c8-fd30-4b22-a05f-dedabf4b1572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 60,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2765a753-0076-43fd-8f74-27996d4a42b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 60,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "65ce5f1b-1312-451d-86c8-fc651f883ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 60,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4dedfec6-68ac-42d7-ae1c-f85b5c880756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 60,
                "offset": -3,
                "shift": 25,
                "w": 31,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "74f71626-3837-4b38-b379-85696499d585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 60,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4822756c-1a38-40a2-9aa2-433ac7d612c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "245be2ff-3078-4925-819c-b8b1ec81e0ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 60,
                "offset": 4,
                "shift": 38,
                "w": 31,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "95b4a18e-f7ec-47bd-a53c-c161967ab0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 60,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3aaad205-a647-464b-8302-f28ea5f79e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 60,
                "offset": 3,
                "shift": 38,
                "w": 30,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "16c4a691-266e-4651-8e76-f46fabea3ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 60,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "64d2f191-a4b6-4b38-9c10-e0b9cddb37a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 60,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 260,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3fc4e32c-3f4a-4744-ad92-b1a76192ddb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 197,
                "y": 64
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "57028011-8ac9-4249-bf1a-543d1da2cdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 473,
                "y": 64
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f0d5d6c6-2f1a-42f3-bad0-4187ac2e2173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 60,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 229,
                "y": 64
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8a86704b-25ee-466b-a8c0-077fa40f8db1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 60,
                "offset": -6,
                "shift": 15,
                "w": 17,
                "x": 257,
                "y": 126
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3cdab492-100b-436f-b128-f95406bc341e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 60,
                "offset": 4,
                "shift": 31,
                "w": 24,
                "x": 231,
                "y": 126
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bfe4c1ee-e5ea-4e3e-aee1-83a06435b0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 60,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 217,
                "y": 126
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bc38d0cd-3764-4a7d-844c-8ec7479d68bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 60,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 172,
                "y": 126
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0c113895-6382-4dfb-9d6d-c86740260282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 142,
                "y": 126
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "24de53f3-12f8-4409-8dbe-f8209cdf17c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 60,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 110,
                "y": 126
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f9aeca9e-ca34-4245-9302-8d6bc25473a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 60,
                "offset": 4,
                "shift": 38,
                "w": 31,
                "x": 77,
                "y": 126
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "deb8505e-464e-4c24-ae46-c55a6b6043f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 60,
                "offset": 3,
                "shift": 38,
                "w": 30,
                "x": 45,
                "y": 126
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f851b25d-91fc-4f8b-a78f-ce4dc9e1f11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 60,
                "offset": 4,
                "shift": 24,
                "w": 20,
                "x": 23,
                "y": 126
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "66ab90d2-cdd9-4e59-84c8-003949cfcc50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 60,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 276,
                "y": 126
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ee498069-7000-4142-bcce-783a78dc504b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 60,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5d1a8b79-cf82-4c91-b8ab-413fb7442c0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 60,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 443,
                "y": 64
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "35c9cb56-6a3b-4441-9892-e08c96285f04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 60,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 412,
                "y": 64
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "733f078f-6393-46a5-b0c2-fe2ff8a16fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 60,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 371,
                "y": 64
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "03a0de9f-adb8-4223-9be7-56ee623868c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 60,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 343,
                "y": 64
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "438e68bc-fc70-41ff-a952-12473be82328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 60,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 311,
                "y": 64
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9c244a5f-b42f-49ee-b475-2847036374db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 60,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 285,
                "y": 64
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3adc0b41-9ed5-4e23-b818-2181c3c98597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 60,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 265,
                "y": 64
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "23f37b58-1924-43d4-ad08-e0860000559f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 60,
                "offset": 8,
                "shift": 21,
                "w": 5,
                "x": 258,
                "y": 64
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "57924466-8f6d-4663-972f-0e29895308f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 60,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 238,
                "y": 64
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "29bd8325-5e8f-468e-beab-be2a944f985c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 60,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 131,
                "y": 312
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9712a548-276a-4c2b-b9a3-2ab20c17068b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 60,
                "offset": 10,
                "shift": 51,
                "w": 31,
                "x": 156,
                "y": 312
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}