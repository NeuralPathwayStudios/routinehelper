{
    "id": "82647b4d-0136-4556-a23a-2e8365109bb4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_50b",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_50b\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6cbccf46-8742-4390-86c0-feed334c9a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 75,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0ed2ef51-9e22-4ee0-b46b-62dd662464b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 75,
                "offset": 3,
                "shift": 19,
                "w": 10,
                "x": 323,
                "y": 156
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "86734225-f65d-4f30-8def-cbee74863415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 75,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 306,
                "y": 156
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "005a4c0e-ef44-460d-903b-cbf1e85e9564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 75,
                "offset": 2,
                "shift": 48,
                "w": 43,
                "x": 261,
                "y": 156
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e1466d82-5899-429e-989a-86c32ac07a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 75,
                "offset": 2,
                "shift": 42,
                "w": 38,
                "x": 221,
                "y": 156
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b7f3f74c-dfcd-4825-861b-f87da6fcd937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 75,
                "offset": 1,
                "shift": 57,
                "w": 55,
                "x": 164,
                "y": 156
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1144292f-e426-4e6b-9236-a0cf7bbfe912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 75,
                "offset": 3,
                "shift": 49,
                "w": 45,
                "x": 117,
                "y": 156
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "899fa7f8-6b79-467f-b5d3-18491bc4dec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 75,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 108,
                "y": 156
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5dc72892-95d1-499c-b0c8-5c886e796c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 75,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 91,
                "y": 156
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "779ef6fc-03d9-4e55-a9be-c859f5638225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 75,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 74,
                "y": 156
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "845431a6-30cd-4912-a7aa-9229f2f6cc04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 75,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 335,
                "y": 156
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6b8eb245-b013-4a2d-9bb6-983f5efbecd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 39,
                "y": 156
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "09ce9fc1-d88b-4879-81df-a87e4ef81090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 75,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 977,
                "y": 79
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ce1b90a8-666f-42e0-b3b0-794dd6bd4f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 75,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 953,
                "y": 79
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f0c2fd15-61c5-4176-9f29-d3a5843e3d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 75,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 941,
                "y": 79
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6b6e7e02-c336-4eec-bfeb-d96f62054006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 75,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 911,
                "y": 79
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "232a625c-c6cb-4db4-8e01-d2431f66f125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 75,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 876,
                "y": 79
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1bba6bfe-d614-4162-b6d6-f73a5c8da092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 75,
                "offset": 2,
                "shift": 26,
                "w": 18,
                "x": 856,
                "y": 79
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "52b07a0c-8b04-4221-ad46-32ea35c409c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 75,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 822,
                "y": 79
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "34e0ba0e-110e-4ef2-a2a6-7dcba052dcf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 75,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 786,
                "y": 79
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5332c23e-f8c3-41f6-8c6d-4e4a27a66600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 75,
                "offset": 1,
                "shift": 44,
                "w": 40,
                "x": 744,
                "y": 79
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "36df5295-dce6-459a-bc03-2eabeba5a4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 75,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 2,
                "y": 156
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4251479a-e31b-44b6-b0ca-986afb148ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 75,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 359,
                "y": 156
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "09dee660-22d8-4efe-8fe5-18499ef5cc51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 393,
                "y": 156
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ad3f2e60-b6dc-432c-a144-fc4acde2139f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 75,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 428,
                "y": 156
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "57d95aa3-fc32-4962-b7d7-bc138ed7372e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 75,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 236,
                "y": 233
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cd3fb19b-aa85-4407-81f8-40a90bcee4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 75,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 223,
                "y": 233
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1b0f4db4-3976-450a-865a-5bd02ca4109c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 75,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 209,
                "y": 233
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bb6feb71-49eb-48c6-bf9d-216b4f84f59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 174,
                "y": 233
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "57f721d1-4542-4b9d-98a8-d8c8c7793921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 75,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 142,
                "y": 233
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e56edf68-0fc1-4101-8685-ba5bd01e1d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 107,
                "y": 233
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "360bd30b-fe48-4198-a2bb-3b2be294dfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 75,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 73,
                "y": 233
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2c775a0c-9f89-45ee-a2ab-13b9110ea101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 75,
                "offset": 2,
                "shift": 73,
                "w": 69,
                "x": 2,
                "y": 233
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d5488e00-2989-4497-ad2a-84291e684c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 75,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 942,
                "y": 156
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "75726f72-1476-4c5c-87be-09f1bb863b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 75,
                "offset": 6,
                "shift": 48,
                "w": 39,
                "x": 901,
                "y": 156
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2af68ab9-a678-4667-8c5b-1d1cb87676d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 75,
                "offset": 4,
                "shift": 52,
                "w": 44,
                "x": 855,
                "y": 156
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "55989b44-c0d3-41aa-9541-eff2ddd6cf52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 75,
                "offset": 6,
                "shift": 52,
                "w": 42,
                "x": 811,
                "y": 156
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e43cb6b0-df8a-4019-a407-801827c76b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 75,
                "offset": 6,
                "shift": 47,
                "w": 38,
                "x": 771,
                "y": 156
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "09be1349-14d3-47c8-b616-a93ad2f37fc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 75,
                "offset": 2,
                "shift": 42,
                "w": 38,
                "x": 731,
                "y": 156
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "db826e8c-2c27-49f7-8230-9221204d5f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 75,
                "offset": 4,
                "shift": 52,
                "w": 44,
                "x": 685,
                "y": 156
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dbf26583-ca46-4f83-a1a6-a6d552c053ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 75,
                "offset": 6,
                "shift": 58,
                "w": 46,
                "x": 637,
                "y": 156
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ed772219-5711-466c-8c8b-a77a3b453aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 75,
                "offset": 6,
                "shift": 20,
                "w": 8,
                "x": 627,
                "y": 156
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4a89ebb4-83d7-4d47-8de0-6866ad649e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 75,
                "offset": 3,
                "shift": 42,
                "w": 33,
                "x": 592,
                "y": 156
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "adc07847-aad4-4198-99eb-dc800bc6721b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 75,
                "offset": 6,
                "shift": 44,
                "w": 36,
                "x": 554,
                "y": 156
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "04560c13-aff0-4f83-ad6a-b81105702469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 75,
                "offset": 6,
                "shift": 43,
                "w": 36,
                "x": 516,
                "y": 156
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e4bbb185-a81f-4319-a898-bfb6b18eec6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 75,
                "offset": 6,
                "shift": 65,
                "w": 53,
                "x": 461,
                "y": 156
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b5b8e75f-f49e-4d45-bc3d-2b2c435762bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 75,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 699,
                "y": 79
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b7f804b9-5d85-4827-8da6-2203ba94e398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 75,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 643,
                "y": 79
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bde7d7d6-00e0-484e-9a9f-08491a8f590b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 75,
                "offset": 6,
                "shift": 42,
                "w": 35,
                "x": 606,
                "y": 79
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d90d80a3-a224-42cc-b04a-96b3c0947676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 75,
                "offset": 4,
                "shift": 61,
                "w": 54,
                "x": 821,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ff225af4-ddbd-4eeb-ae3b-445b45e04ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 75,
                "offset": 6,
                "shift": 42,
                "w": 36,
                "x": 763,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0f93ffbf-6a95-412d-ac38-49c8c73fbccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 75,
                "offset": 3,
                "shift": 46,
                "w": 39,
                "x": 722,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5a62a7f1-0101-4666-97da-45b37430eaa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 75,
                "offset": 1,
                "shift": 44,
                "w": 43,
                "x": 677,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e15daf90-2152-44c1-9efb-5d5d6ffa0d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 75,
                "offset": 6,
                "shift": 56,
                "w": 44,
                "x": 631,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0d32c1c4-3c2e-4f5a-91be-d769a53e9346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 75,
                "offset": 0,
                "shift": 49,
                "w": 49,
                "x": 580,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3fd54f49-437e-4b44-b4a2-5989056dd562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 75,
                "offset": 0,
                "shift": 63,
                "w": 63,
                "x": 515,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "720acfe2-6028-4e08-979d-21026f6575f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 75,
                "offset": 3,
                "shift": 48,
                "w": 42,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "16f1f680-e84f-4b8d-b28e-755f93adf4f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 75,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cc3980b8-ef58-47a0-987b-8c1779466c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 75,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "93e54381-5ca7-463b-9c8d-b9a05d2931e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 75,
                "offset": 4,
                "shift": 21,
                "w": 18,
                "x": 801,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a360c925-94dc-4a80-920f-f69c9a310c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 75,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8d5e78e2-52b0-4879-93f3-830f30314f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 75,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8e141263-1dc6-4ba6-92e8-89afd96aa18a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 75,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "94dd0658-8410-4843-8515-16e174798a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 75,
                "offset": -3,
                "shift": 31,
                "w": 39,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bcfe4062-340c-4fde-9538-ec69bc8a6d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 75,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "97a3e29e-782c-4e03-918d-4db19af5dbeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 75,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1ae9774e-e94c-4678-a4ee-95a3e97cf3a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 75,
                "offset": 6,
                "shift": 47,
                "w": 37,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "24a3a02e-7adb-4de0-b7a1-49299e8e95ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 75,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "78f5171b-237b-46b4-967c-d863e37764d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 75,
                "offset": 4,
                "shift": 47,
                "w": 37,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f9f8f4dd-fa22-43aa-b888-83829b586750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 75,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9e27c94b-b9cd-4918-ab5c-ad38353c6604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 75,
                "offset": 0,
                "shift": 24,
                "w": 26,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "498f365c-7d4c-4828-bc7f-2ed20d25deb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 75,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 877,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "02627774-50cd-4ffe-92fe-9b5b643301e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 75,
                "offset": 6,
                "shift": 45,
                "w": 35,
                "x": 202,
                "y": 79
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7b2b3ae9-cbfe-49ed-82ed-16acad33695e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 75,
                "offset": 5,
                "shift": 20,
                "w": 10,
                "x": 916,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bfc624b4-35fb-45ca-9207-d68cf2878459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 75,
                "offset": -7,
                "shift": 19,
                "w": 21,
                "x": 550,
                "y": 79
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "aad4c626-e5d8-45ff-8ac8-0473b58b5da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 75,
                "offset": 6,
                "shift": 38,
                "w": 29,
                "x": 519,
                "y": 79
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a5973fb4-e584-47ff-a553-0445fba7fff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 75,
                "offset": 4,
                "shift": 20,
                "w": 16,
                "x": 501,
                "y": 79
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e1c946a0-a7d8-4740-96cf-f816adcabe68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 75,
                "offset": 6,
                "shift": 63,
                "w": 52,
                "x": 447,
                "y": 79
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "286645ea-560a-41fd-bba9-336b34df32fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 75,
                "offset": 6,
                "shift": 45,
                "w": 35,
                "x": 410,
                "y": 79
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8d982cd7-29df-422a-b5db-79fe7702654a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 75,
                "offset": 4,
                "shift": 45,
                "w": 37,
                "x": 371,
                "y": 79
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9afa8c86-127b-4603-93a7-d9a4f042440c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 75,
                "offset": 6,
                "shift": 47,
                "w": 37,
                "x": 332,
                "y": 79
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0839db25-3b6f-4be4-8611-9680fd9f918d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 75,
                "offset": 4,
                "shift": 47,
                "w": 37,
                "x": 293,
                "y": 79
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "28d4fd19-05fd-4a74-a5a8-1363a1522393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 75,
                "offset": 6,
                "shift": 32,
                "w": 26,
                "x": 265,
                "y": 79
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "695d23d7-b830-46f6-9f53-ce0aa55b4e4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 573,
                "y": 79
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "04941588-9943-425c-a5de-9e16d75b9f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 75,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 239,
                "y": 79
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "031e598c-a174-4920-be00-60bc152d29d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 75,
                "offset": 5,
                "shift": 45,
                "w": 35,
                "x": 165,
                "y": 79
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c91ecbdb-ebde-45a8-b170-4cc11166f807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 75,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 126,
                "y": 79
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fc609aca-bef7-44f2-b2fa-b0d1ed2e4dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 75,
                "offset": 0,
                "shift": 49,
                "w": 48,
                "x": 76,
                "y": 79
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ba348126-fd45-40c0-b029-708127a31bdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 75,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 41,
                "y": 79
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8e30c7ab-9c18-436b-8525-561a8c76b30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 75,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 2,
                "y": 79
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b5a7120e-a5eb-4870-ab49-3609c0b0c120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 75,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 986,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "94dfa488-8ad4-4186-b3ff-8694fe244366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 75,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 962,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3778422e-e20b-4453-a28e-09d8cab8758c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 75,
                "offset": 9,
                "shift": 26,
                "w": 8,
                "x": 952,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0989dc6d-b0ed-4b99-a3d5-819a47cb743e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 75,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 928,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b393d2b1-4fb4-4a77-92f2-821bc0b7a853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 75,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 270,
                "y": 233
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "76894d3f-290c-4a16-8ffa-b6fa278c9f46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 75,
                "offset": 13,
                "shift": 65,
                "w": 39,
                "x": 300,
                "y": 233
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 50,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}