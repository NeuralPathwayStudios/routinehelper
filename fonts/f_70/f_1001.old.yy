{
    "id": "f7c30c1f-9dc1-4b29-9f85-4b6b4bd334b1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_100",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_100\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e8e04c14-d93a-4bf3-85b2-6989cffc8402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 150,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "41dddc22-b49a-4a2e-b0eb-c8090492b1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 150,
                "offset": 6,
                "shift": 35,
                "w": 17,
                "x": 299,
                "y": 306
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "00b57703-377b-4d9e-a3cc-9361f5f97bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 150,
                "offset": 5,
                "shift": 36,
                "w": 27,
                "x": 270,
                "y": 306
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3e174f01-8f6d-442a-ae71-cc47ac900b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 150,
                "offset": 6,
                "shift": 97,
                "w": 85,
                "x": 183,
                "y": 306
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b1b1cd64-f8b1-42a6-b24b-87c7d468ec74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 150,
                "offset": 5,
                "shift": 80,
                "w": 69,
                "x": 112,
                "y": 306
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "01bc2e29-20d6-41a7-8801-7bf1d32dd158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 150,
                "offset": 4,
                "shift": 115,
                "w": 108,
                "x": 2,
                "y": 306
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0ed0fced-29b8-4deb-b655-b08f85b4ef80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 150,
                "offset": 6,
                "shift": 96,
                "w": 85,
                "x": 1932,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d568703f-a1a0-4c61-86c5-9f9631c32cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 150,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 1919,
                "y": 154
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8b0cee78-814a-4d84-ad64-b8d152e14310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 150,
                "offset": 7,
                "shift": 34,
                "w": 26,
                "x": 1891,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b69eb891-41c0-46ed-8931-e4b6c8cbd4e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 150,
                "offset": 1,
                "shift": 34,
                "w": 27,
                "x": 1862,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5d5be299-8aca-4f3e-bf5b-c7b0411b79f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 150,
                "offset": 6,
                "shift": 51,
                "w": 40,
                "x": 318,
                "y": 306
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "44268cea-0c00-416e-b2c4-18f28e9c6898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1797,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a100c477-83f8-468b-b503-7c2ca3cd0647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 150,
                "offset": 6,
                "shift": 30,
                "w": 18,
                "x": 1706,
                "y": 154
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9b158a7d-9f5a-4b43-af64-5e6438f2b0ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 150,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 1662,
                "y": 154
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "48a472be-4001-4c21-9bf3-78e0671459f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 150,
                "offset": 6,
                "shift": 29,
                "w": 17,
                "x": 1643,
                "y": 154
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bfe615c6-27da-4d67-af18-df5da86932de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 150,
                "offset": 4,
                "shift": 63,
                "w": 54,
                "x": 1587,
                "y": 154
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "90a33c12-b181-4eab-83d0-37cd321dc085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 150,
                "offset": 6,
                "shift": 76,
                "w": 64,
                "x": 1521,
                "y": 154
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "32301b3b-3a74-40be-9a5e-6d5bf7f1be20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 150,
                "offset": 5,
                "shift": 50,
                "w": 33,
                "x": 1486,
                "y": 154
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ac71158d-7f41-40db-8d48-78e6f79d7f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 150,
                "offset": 8,
                "shift": 77,
                "w": 64,
                "x": 1420,
                "y": 154
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a9fa2739-e3a7-4344-94fc-7037d7edd2c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 150,
                "offset": 7,
                "shift": 78,
                "w": 65,
                "x": 1353,
                "y": 154
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a0c492ec-697d-4f8f-bbfd-2546706e51ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 150,
                "offset": 1,
                "shift": 86,
                "w": 80,
                "x": 1271,
                "y": 154
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7ebfe24e-274e-4b08-b20a-5ab8ba816719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 150,
                "offset": 8,
                "shift": 84,
                "w": 69,
                "x": 1726,
                "y": 154
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5d269db5-a864-4fe1-8b88-ecb8413626fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 150,
                "offset": 8,
                "shift": 77,
                "w": 64,
                "x": 360,
                "y": 306
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8828bc63-001b-48d2-9669-48b50ab9a5d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 150,
                "offset": 6,
                "shift": 76,
                "w": 65,
                "x": 426,
                "y": 306
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "616d5fae-1e13-4f8c-aa2a-3d0f68ac9085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 150,
                "offset": 8,
                "shift": 79,
                "w": 63,
                "x": 493,
                "y": 306
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3796d162-2744-44b2-9275-b5c2aa5e69f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 150,
                "offset": 6,
                "shift": 77,
                "w": 63,
                "x": 2,
                "y": 458
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c5ad520b-64ae-442d-bf6b-a1167b4f8b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 150,
                "offset": 4,
                "shift": 26,
                "w": 16,
                "x": 1971,
                "y": 306
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a7876d66-215b-41a9-bfed-23b1ab50510e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 150,
                "offset": 4,
                "shift": 27,
                "w": 20,
                "x": 1949,
                "y": 306
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ab36cb50-34df-41b3-8c77-f033f06b90dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1884,
                "y": 306
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "34a76246-c5a6-4fe7-b4bd-7269889490cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 150,
                "offset": 5,
                "shift": 68,
                "w": 58,
                "x": 1824,
                "y": 306
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "82fc6e08-829e-431e-9a37-f3452b9f92f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1759,
                "y": 306
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "764c29d0-19f8-4e94-af66-d761998849e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1694,
                "y": 306
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "654a56b5-1f0d-4849-9a12-3bee82688cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 150,
                "offset": 5,
                "shift": 146,
                "w": 136,
                "x": 1556,
                "y": 306
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "19e06218-da4f-4560-abe3-959280c5e7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 150,
                "offset": 1,
                "shift": 94,
                "w": 92,
                "x": 1462,
                "y": 306
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f65c791d-9551-4f30-b639-b3a9daa07a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 150,
                "offset": 13,
                "shift": 93,
                "w": 73,
                "x": 1387,
                "y": 306
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3300a73a-0b5d-4734-ae08-cf82ee7cb73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 150,
                "offset": 8,
                "shift": 103,
                "w": 87,
                "x": 1298,
                "y": 306
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c9aca2a9-d98c-4d50-a115-ce25dafe4b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 150,
                "offset": 13,
                "shift": 103,
                "w": 82,
                "x": 1214,
                "y": 306
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1444a3c0-40e3-4f74-bb8a-1b9ab1dcd1b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 150,
                "offset": 13,
                "shift": 92,
                "w": 74,
                "x": 1138,
                "y": 306
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ed11fc8e-9df4-43a3-a07a-cf8880b95ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 150,
                "offset": 5,
                "shift": 83,
                "w": 73,
                "x": 1063,
                "y": 306
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "feddcef1-beaa-498f-93fe-974688bfb636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 150,
                "offset": 8,
                "shift": 103,
                "w": 87,
                "x": 974,
                "y": 306
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e1bc82b2-54b6-4f6c-948b-487d3c5810c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 150,
                "offset": 13,
                "shift": 113,
                "w": 87,
                "x": 885,
                "y": 306
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "47ddcddd-db23-4c9f-9395-e415610ad756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 150,
                "offset": 13,
                "shift": 37,
                "w": 11,
                "x": 872,
                "y": 306
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ce1201be-bbb2-4e31-b617-122eb76ba49b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 150,
                "offset": 6,
                "shift": 82,
                "w": 63,
                "x": 807,
                "y": 306
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e982ef3f-5577-4b73-b75e-c8f417ef8ac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 150,
                "offset": 13,
                "shift": 87,
                "w": 69,
                "x": 736,
                "y": 306
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5a8f0473-7964-4105-bc56-20f6ed71204b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 150,
                "offset": 13,
                "shift": 84,
                "w": 69,
                "x": 665,
                "y": 306
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "01c50974-3c9d-4e40-a220-c835b7ff3006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 150,
                "offset": 13,
                "shift": 131,
                "w": 105,
                "x": 558,
                "y": 306
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0ba89b60-dee8-4913-8cd1-8d8aa1e0043a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 150,
                "offset": 13,
                "shift": 110,
                "w": 84,
                "x": 1185,
                "y": 154
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "76bc76d7-7b53-444e-82fe-4102be45541e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 150,
                "offset": 8,
                "shift": 123,
                "w": 107,
                "x": 1076,
                "y": 154
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "cb162f80-0d2b-4771-9150-2953277df82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 150,
                "offset": 13,
                "shift": 82,
                "w": 66,
                "x": 1008,
                "y": 154
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e1cbd496-307b-472d-a2a5-0c4d44be1fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 150,
                "offset": 8,
                "shift": 123,
                "w": 107,
                "x": 1539,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b4cdef2c-1c64-47fa-9c3d-781d6c2903d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 150,
                "offset": 13,
                "shift": 82,
                "w": 68,
                "x": 1434,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "856f8bd3-2598-4444-a4ce-cb9e18468dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 150,
                "offset": 7,
                "shift": 88,
                "w": 73,
                "x": 1359,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "352ce5e4-60c6-4f82-87a1-437ff9ec7ec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 150,
                "offset": 2,
                "shift": 87,
                "w": 84,
                "x": 1273,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dd97d0a4-0ff5-4491-b3c8-f4d307f0d012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 150,
                "offset": 13,
                "shift": 109,
                "w": 83,
                "x": 1188,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fde6e897-012e-4fee-94f9-718f7bb9f2a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 150,
                "offset": 2,
                "shift": 95,
                "w": 92,
                "x": 1094,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4eec27ac-fd4d-4d35-9a1a-666c855432d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 150,
                "offset": 2,
                "shift": 125,
                "w": 121,
                "x": 971,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5a45a29a-5707-4608-b7cc-0a7a222e406c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 150,
                "offset": 6,
                "shift": 94,
                "w": 83,
                "x": 886,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2e87cc08-f1be-4f62-89d2-ae1b4a9603dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 150,
                "offset": 0,
                "shift": 86,
                "w": 86,
                "x": 798,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "03d1172b-5259-4f5a-9656-3a1f15c2bf2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 150,
                "offset": 6,
                "shift": 97,
                "w": 85,
                "x": 711,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d378b098-9644-4cd8-9a23-8576c223b786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 150,
                "offset": 8,
                "shift": 40,
                "w": 33,
                "x": 1504,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "485f1cb9-4e34-4b42-a8e3-2422dc291a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 150,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 656,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ea9508ef-baf1-4ae0-ac5c-7a273800e51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 150,
                "offset": 0,
                "shift": 40,
                "w": 32,
                "x": 572,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "490dc3ed-ec0a-4c85-8558-fce98465c4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 150,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 517,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ab8d255e-d20f-412c-aca6-ccca0ea53d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 150,
                "offset": -6,
                "shift": 63,
                "w": 76,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "dc404040-bed9-4e67-a72e-29b859c6b15d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 150,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "da038df5-dc30-4b70-9172-a82d8ae2dd5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 150,
                "offset": 8,
                "shift": 90,
                "w": 75,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8a0e3565-7ea2-46f7-a4f3-96717954fb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 150,
                "offset": 12,
                "shift": 94,
                "w": 75,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b8b2c890-0952-4b9a-b4cd-155c9260babd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 150,
                "offset": 8,
                "shift": 79,
                "w": 64,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "defc99e0-9a5b-4e11-bc1b-e41da96d97a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 150,
                "offset": 8,
                "shift": 94,
                "w": 75,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4fafb7d1-a3cf-48bb-ae90-1656c69e3b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 150,
                "offset": 8,
                "shift": 82,
                "w": 69,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d3d95112-c7a5-45c4-b30d-f428e5653f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 150,
                "offset": 2,
                "shift": 49,
                "w": 48,
                "x": 606,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "db74c11a-fcf4-4667-8daa-2761d94ce45f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 150,
                "offset": 8,
                "shift": 90,
                "w": 75,
                "x": 1648,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2186a10a-edd6-43a4-a849-0822ba649e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 150,
                "offset": 12,
                "shift": 90,
                "w": 68,
                "x": 243,
                "y": 154
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d09d4d80-4d14-48f6-abe8-8648d2bcd671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 150,
                "offset": 12,
                "shift": 39,
                "w": 15,
                "x": 1725,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4dca0725-f136-4803-aafd-3b566ab28a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 150,
                "offset": -13,
                "shift": 38,
                "w": 39,
                "x": 905,
                "y": 154
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fc675d0e-2096-4c85-8359-6ac988c7aa67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 150,
                "offset": 12,
                "shift": 76,
                "w": 57,
                "x": 846,
                "y": 154
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5f661dc0-8e45-4ab8-b16a-d999b9eeeb73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 150,
                "offset": 9,
                "shift": 38,
                "w": 27,
                "x": 817,
                "y": 154
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5f75e7aa-e1ca-4a44-b61a-c1bae267873b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 150,
                "offset": 12,
                "shift": 126,
                "w": 104,
                "x": 711,
                "y": 154
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0816734a-aaf7-44a4-ac3d-468e258f9fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 150,
                "offset": 12,
                "shift": 90,
                "w": 68,
                "x": 641,
                "y": 154
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8b8aa070-7359-4930-becf-a770ce060c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 150,
                "offset": 8,
                "shift": 90,
                "w": 75,
                "x": 564,
                "y": 154
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5c6129f6-b38c-405c-a9cf-a51acb39b8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 150,
                "offset": 12,
                "shift": 94,
                "w": 75,
                "x": 487,
                "y": 154
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "968e51cb-aadb-4615-a5e9-1bc67f11b71b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 150,
                "offset": 8,
                "shift": 94,
                "w": 75,
                "x": 410,
                "y": 154
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "61955385-2608-4c28-bd5d-724baed96fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 150,
                "offset": 12,
                "shift": 61,
                "w": 46,
                "x": 362,
                "y": 154
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "de860e09-0a07-4156-b8da-528a65151a2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 150,
                "offset": 7,
                "shift": 72,
                "w": 60,
                "x": 946,
                "y": 154
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b060c0c6-757f-4342-b811-97c3f3b73217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 150,
                "offset": 2,
                "shift": 54,
                "w": 47,
                "x": 313,
                "y": 154
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9e77b638-953a-4d8c-9e48-5962044729c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 150,
                "offset": 10,
                "shift": 90,
                "w": 69,
                "x": 172,
                "y": 154
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "96f2932f-c9bf-4af7-a07b-b915a44723f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 150,
                "offset": 1,
                "shift": 74,
                "w": 71,
                "x": 99,
                "y": 154
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0e07bbb6-34b8-46f2-aaaf-10a838767bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 150,
                "offset": 2,
                "shift": 99,
                "w": 95,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "24eb7de6-5e5a-4a97-8294-b31dc2ec90e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1981,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c7a07011-ad8c-4cfe-bbce-22186a666163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 150,
                "offset": 2,
                "shift": 73,
                "w": 72,
                "x": 1907,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "60a023e5-2bc6-4925-b9ed-411112b5c1d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 150,
                "offset": 8,
                "shift": 73,
                "w": 60,
                "x": 1845,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "34b5a24f-0695-4e4d-b50c-840968075cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 150,
                "offset": 7,
                "shift": 50,
                "w": 42,
                "x": 1801,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ff6de400-9b52-400e-a9d1-49be4fbcff9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 150,
                "offset": 20,
                "shift": 52,
                "w": 12,
                "x": 1787,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ab5b44dd-f5df-463c-9702-b927197271e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 150,
                "offset": 1,
                "shift": 50,
                "w": 43,
                "x": 1742,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d8e10065-7312-44d1-be45-34c406814838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 150,
                "offset": 4,
                "shift": 63,
                "w": 56,
                "x": 67,
                "y": 458
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e1c8e855-e651-47a3-ab9f-99753d9ca430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 150,
                "offset": 26,
                "shift": 129,
                "w": 77,
                "x": 125,
                "y": 458
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 100,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}