{
    "id": "cf4a3326-9341-49d7-9ddd-ce10d6a3eabf",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_70",
    "AntiAlias": 1,
    "TTFName": "fonts\\f_70\\Comfortaa-VariableFont_wght.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comfortaa",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e6ca1df4-7806-415a-bffe-d6de29d125f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 150,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "539fea48-c2a0-4ba5-aa07-14e969b46b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 150,
                "offset": 6,
                "shift": 35,
                "w": 17,
                "x": 299,
                "y": 306
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9240b938-080e-4b20-b002-b5658056792c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 150,
                "offset": 5,
                "shift": 36,
                "w": 27,
                "x": 270,
                "y": 306
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ee7c0a8a-09be-4ed4-94b5-487240c22479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 150,
                "offset": 6,
                "shift": 97,
                "w": 85,
                "x": 183,
                "y": 306
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a70e4ca0-b343-489f-a1a9-5bd537c5c15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 150,
                "offset": 5,
                "shift": 80,
                "w": 69,
                "x": 112,
                "y": 306
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8ba36271-8135-443d-ad56-b31704ba0f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 150,
                "offset": 4,
                "shift": 115,
                "w": 108,
                "x": 2,
                "y": 306
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f73902ce-f719-4bf7-9358-2517cd5b3054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 150,
                "offset": 6,
                "shift": 96,
                "w": 85,
                "x": 1932,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0f304fb4-69dd-4da3-8eca-9c0794244a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 150,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 1919,
                "y": 154
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f433441b-8644-4537-86ca-b4bedfd8b9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 150,
                "offset": 7,
                "shift": 34,
                "w": 26,
                "x": 1891,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cf4b25e2-2e68-444d-b039-aae778404931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 150,
                "offset": 1,
                "shift": 34,
                "w": 27,
                "x": 1862,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9e102f64-af3d-44c7-90d2-038d47092b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 150,
                "offset": 6,
                "shift": 51,
                "w": 40,
                "x": 318,
                "y": 306
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a76684c9-1131-4ac6-877f-61b203bc39ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1797,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "99f32521-d126-4f14-8df4-13c59e93caa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 150,
                "offset": 6,
                "shift": 30,
                "w": 18,
                "x": 1706,
                "y": 154
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "85bde291-8884-4072-aebd-bd7bcb9c3f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 150,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 1662,
                "y": 154
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b0aab5ea-4ae2-4a13-8218-da86f37ef060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 150,
                "offset": 6,
                "shift": 29,
                "w": 17,
                "x": 1643,
                "y": 154
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "853736de-cd53-432c-80f5-aab447e63b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 150,
                "offset": 4,
                "shift": 63,
                "w": 54,
                "x": 1587,
                "y": 154
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "efcdace9-d380-440d-9e8e-ea9b755b51b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 150,
                "offset": 6,
                "shift": 76,
                "w": 64,
                "x": 1521,
                "y": 154
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fa63faf4-8c74-4a8d-a3f4-01d435963721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 150,
                "offset": 5,
                "shift": 50,
                "w": 33,
                "x": 1486,
                "y": 154
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ee409568-f67f-4379-acee-d47963e4923f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 150,
                "offset": 8,
                "shift": 77,
                "w": 64,
                "x": 1420,
                "y": 154
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4682f46f-1573-4f74-a83f-336000f97e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 150,
                "offset": 7,
                "shift": 78,
                "w": 65,
                "x": 1353,
                "y": 154
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d2ec39f3-d8ae-4fc6-abb5-c5e6c8e72208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 150,
                "offset": 1,
                "shift": 86,
                "w": 80,
                "x": 1271,
                "y": 154
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "761c3b86-1d7f-4802-9b86-2b2aca86d21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 150,
                "offset": 8,
                "shift": 84,
                "w": 69,
                "x": 1726,
                "y": 154
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ffec91c3-0e4a-43f4-b67f-28d244539b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 150,
                "offset": 8,
                "shift": 77,
                "w": 64,
                "x": 360,
                "y": 306
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fc09ef0f-c1e1-4f02-8477-2aa0b9ac2c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 150,
                "offset": 6,
                "shift": 76,
                "w": 65,
                "x": 426,
                "y": 306
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "059ae81c-ca44-4224-9a01-a3d00d9c8ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 150,
                "offset": 8,
                "shift": 79,
                "w": 63,
                "x": 493,
                "y": 306
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0a5f3498-1f84-4a5f-b9d3-584878dff8a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 150,
                "offset": 6,
                "shift": 77,
                "w": 63,
                "x": 2,
                "y": 458
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "472a5140-72e2-4f8d-993d-4cb5fc137541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 150,
                "offset": 4,
                "shift": 26,
                "w": 16,
                "x": 1971,
                "y": 306
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3576382b-b440-498b-a8a2-2b4a84924780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 150,
                "offset": 4,
                "shift": 27,
                "w": 20,
                "x": 1949,
                "y": 306
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "90a027d7-6014-40a5-b180-16f317b0accb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1884,
                "y": 306
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0baac291-e7aa-4fe4-a748-a9803fec2435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 150,
                "offset": 5,
                "shift": 68,
                "w": 58,
                "x": 1824,
                "y": 306
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b8a95a10-6fd1-4d58-9518-663a6e5e1be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1759,
                "y": 306
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "016d1f38-2302-4d1b-a216-636c281f543e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1694,
                "y": 306
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "931a6f44-aaf8-4f56-b52c-ef77bb75cc04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 150,
                "offset": 5,
                "shift": 146,
                "w": 136,
                "x": 1556,
                "y": 306
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "efb74988-8e01-4173-a4c7-918a4fcf69c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 150,
                "offset": 1,
                "shift": 94,
                "w": 92,
                "x": 1462,
                "y": 306
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "05a38a4e-2aba-4874-979f-e14910b50cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 150,
                "offset": 13,
                "shift": 93,
                "w": 73,
                "x": 1387,
                "y": 306
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "033fc3c2-4a03-4c17-a9e5-abce2272fe5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 150,
                "offset": 8,
                "shift": 103,
                "w": 87,
                "x": 1298,
                "y": 306
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ce6e250d-1ffb-4882-8135-8e3e2901937d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 150,
                "offset": 13,
                "shift": 103,
                "w": 82,
                "x": 1214,
                "y": 306
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "84e714bc-2160-4081-b02a-52b72028133b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 150,
                "offset": 13,
                "shift": 92,
                "w": 74,
                "x": 1138,
                "y": 306
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "097b55aa-8a0e-40a4-89be-d082abb4c78e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 150,
                "offset": 5,
                "shift": 83,
                "w": 73,
                "x": 1063,
                "y": 306
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "22c24374-a2ce-4fa9-ac56-02629815ebfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 150,
                "offset": 8,
                "shift": 103,
                "w": 87,
                "x": 974,
                "y": 306
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8077e5d3-ae10-4954-afc3-bf87d4242314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 150,
                "offset": 13,
                "shift": 113,
                "w": 87,
                "x": 885,
                "y": 306
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b768d103-094e-4469-bcd4-63152a6b15ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 150,
                "offset": 13,
                "shift": 37,
                "w": 11,
                "x": 872,
                "y": 306
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3e36a45d-d64e-4ea6-95fc-e7672b031c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 150,
                "offset": 6,
                "shift": 82,
                "w": 63,
                "x": 807,
                "y": 306
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d82aae88-e0d7-4601-a725-752c4498c0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 150,
                "offset": 13,
                "shift": 87,
                "w": 69,
                "x": 736,
                "y": 306
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9f7f0c4a-4aec-40e5-bd5f-f2151ca02090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 150,
                "offset": 13,
                "shift": 84,
                "w": 69,
                "x": 665,
                "y": 306
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "846ed51d-8b31-4e60-8ccf-39291c537823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 150,
                "offset": 13,
                "shift": 131,
                "w": 105,
                "x": 558,
                "y": 306
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2ab200e9-2f5c-4363-a8fe-4c0e0bd701c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 150,
                "offset": 13,
                "shift": 110,
                "w": 84,
                "x": 1185,
                "y": 154
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "298d5894-0e5f-4d39-a5e5-5826655ed20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 150,
                "offset": 8,
                "shift": 123,
                "w": 107,
                "x": 1076,
                "y": 154
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9789871c-d106-4a24-b0cc-9da3ad6df70a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 150,
                "offset": 13,
                "shift": 82,
                "w": 66,
                "x": 1008,
                "y": 154
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ead8a71f-1c39-4b18-89be-b795aada4c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 150,
                "offset": 8,
                "shift": 123,
                "w": 107,
                "x": 1539,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "92584d72-d774-4581-844e-8ba8f07ea3e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 150,
                "offset": 13,
                "shift": 82,
                "w": 68,
                "x": 1434,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6f29129a-1b87-43d7-9c70-2fceab9b36a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 150,
                "offset": 7,
                "shift": 88,
                "w": 73,
                "x": 1359,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "06f5725e-efa9-48d3-adea-d0e40ca141cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 150,
                "offset": 2,
                "shift": 87,
                "w": 84,
                "x": 1273,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "72fa4cb3-55cb-48a7-b9b6-421551df9af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 150,
                "offset": 13,
                "shift": 109,
                "w": 83,
                "x": 1188,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "007847ce-1ced-488d-8c4a-e79b8a43750e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 150,
                "offset": 2,
                "shift": 95,
                "w": 92,
                "x": 1094,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e8110863-3029-4176-9d6e-eb11229fce9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 150,
                "offset": 2,
                "shift": 125,
                "w": 121,
                "x": 971,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "69a9ed01-18f6-47d2-b3fb-f2e26f1a843e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 150,
                "offset": 6,
                "shift": 94,
                "w": 83,
                "x": 886,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cd56a13a-34af-4474-b573-8117ec9465ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 150,
                "offset": 0,
                "shift": 86,
                "w": 86,
                "x": 798,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "91504b60-7b24-4101-a009-0970ac484a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 150,
                "offset": 6,
                "shift": 97,
                "w": 85,
                "x": 711,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b053728b-9617-4f80-9e65-d594e5fa282e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 150,
                "offset": 8,
                "shift": 40,
                "w": 33,
                "x": 1504,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3511cc24-e0b7-4e70-9bcc-991ab1c1c2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 150,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 656,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f6c9f467-360e-41ad-aa81-99f232070e1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 150,
                "offset": 0,
                "shift": 40,
                "w": 32,
                "x": 572,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f3f45a93-bce0-4934-bff0-4eb575816f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 150,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 517,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cf641e47-0493-40f9-aa25-0f360c9de5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 150,
                "offset": -6,
                "shift": 63,
                "w": 76,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1fd1f6a8-6dab-49d3-8ec5-c67b680270a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 150,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d78cc618-8901-4059-8bb9-801812ecd806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 150,
                "offset": 8,
                "shift": 90,
                "w": 75,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "62ed4bce-4acb-409d-a7c1-6878c72686f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 150,
                "offset": 12,
                "shift": 94,
                "w": 75,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "30891867-90fc-4702-b801-02429ebb2f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 150,
                "offset": 8,
                "shift": 79,
                "w": 64,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "643d3732-bc85-47e9-85f1-a1591c5c409f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 150,
                "offset": 8,
                "shift": 94,
                "w": 75,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "36e0d599-9c76-406d-a113-5cadde4850b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 150,
                "offset": 8,
                "shift": 82,
                "w": 69,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e227a79a-9714-46e1-b999-5a6fc7046fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 150,
                "offset": 2,
                "shift": 49,
                "w": 48,
                "x": 606,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6a313029-d700-4bd3-b097-936f8bf9d8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 150,
                "offset": 8,
                "shift": 90,
                "w": 75,
                "x": 1648,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fbcb142a-b7f2-4ffd-82e6-370335792a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 150,
                "offset": 12,
                "shift": 90,
                "w": 68,
                "x": 243,
                "y": 154
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1cb8cdaf-8f5e-490c-8ac8-e6ea70c86b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 150,
                "offset": 12,
                "shift": 39,
                "w": 15,
                "x": 1725,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7aa5f90d-7961-4593-b8dc-cdf61401fd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 150,
                "offset": -13,
                "shift": 38,
                "w": 39,
                "x": 905,
                "y": 154
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "554e6cb0-fc69-4550-bbd2-0fcd4846e2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 150,
                "offset": 12,
                "shift": 76,
                "w": 57,
                "x": 846,
                "y": 154
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f0730013-5626-41d3-8041-87a0c73e8b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 150,
                "offset": 9,
                "shift": 38,
                "w": 27,
                "x": 817,
                "y": 154
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d8983bda-f0d0-4f1f-834f-cbe2c903b062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 150,
                "offset": 12,
                "shift": 126,
                "w": 104,
                "x": 711,
                "y": 154
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c589ea50-2ccd-4106-a516-a657f8ac0568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 150,
                "offset": 12,
                "shift": 90,
                "w": 68,
                "x": 641,
                "y": 154
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8196d851-ef4f-4972-8463-112fad048ff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 150,
                "offset": 8,
                "shift": 90,
                "w": 75,
                "x": 564,
                "y": 154
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "80a96230-aca0-49a9-897f-fe6b942fc386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 150,
                "offset": 12,
                "shift": 94,
                "w": 75,
                "x": 487,
                "y": 154
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "afe76df2-3158-4f15-b82a-ed261901ca98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 150,
                "offset": 8,
                "shift": 94,
                "w": 75,
                "x": 410,
                "y": 154
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "855611bc-eb62-4034-b586-b350accf9b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 150,
                "offset": 12,
                "shift": 61,
                "w": 46,
                "x": 362,
                "y": 154
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "42687fa8-4f36-49b9-918f-989e3a467e4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 150,
                "offset": 7,
                "shift": 72,
                "w": 60,
                "x": 946,
                "y": 154
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e5d7c6be-d54d-44ec-98e3-b042f588de5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 150,
                "offset": 2,
                "shift": 54,
                "w": 47,
                "x": 313,
                "y": 154
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7071f07c-d350-4992-9574-f342ea876273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 150,
                "offset": 10,
                "shift": 90,
                "w": 69,
                "x": 172,
                "y": 154
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c1441025-c1a1-4e80-92e0-4f2528ec6ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 150,
                "offset": 1,
                "shift": 74,
                "w": 71,
                "x": 99,
                "y": 154
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a8c6b968-b958-4d82-a689-ee1960ddd20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 150,
                "offset": 2,
                "shift": 99,
                "w": 95,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c43df05c-58ae-4408-b2b8-af90f5b90c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 150,
                "offset": 5,
                "shift": 73,
                "w": 63,
                "x": 1981,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6937c8a5-8488-4e0e-ac1c-7b3a423d87ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 150,
                "offset": 2,
                "shift": 73,
                "w": 72,
                "x": 1907,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5f207b64-d95b-4fbf-b0c6-0aea5a397a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 150,
                "offset": 8,
                "shift": 73,
                "w": 60,
                "x": 1845,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d2cabae7-cddf-47d1-8728-866336ec3618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 150,
                "offset": 7,
                "shift": 50,
                "w": 42,
                "x": 1801,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4a0491f9-c748-4712-aa0e-153db8d33024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 150,
                "offset": 20,
                "shift": 52,
                "w": 12,
                "x": 1787,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "00a5e315-a347-4e5b-96dd-7dd1712ecdb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 150,
                "offset": 1,
                "shift": 50,
                "w": 43,
                "x": 1742,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "03dd7207-7c15-4089-a57f-617fd0f6f79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 150,
                "offset": 4,
                "shift": 63,
                "w": 56,
                "x": 67,
                "y": 458
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "63ec0b2c-52f1-49f0-bd78-475c7df1bd32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 150,
                "offset": 26,
                "shift": 129,
                "w": 77,
                "x": 125,
                "y": 458
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 100,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}