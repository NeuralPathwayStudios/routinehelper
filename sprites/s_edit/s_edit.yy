{
    "id": "ca72f4ac-777b-42bb-98c0-530318028f6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_edit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f33d6893-b0f1-4aee-921b-350978ef1ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca72f4ac-777b-42bb-98c0-530318028f6b",
            "compositeImage": {
                "id": "bde5c3ea-2198-4b63-895b-b3455d5ba61b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f33d6893-b0f1-4aee-921b-350978ef1ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505b548c-1d32-48e6-b996-6ed0772d0d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f33d6893-b0f1-4aee-921b-350978ef1ee4",
                    "LayerId": "c33fce13-3623-4ad7-ab3e-2bd4595b0064"
                }
            ]
        }
    ],
    "gridX": 25,
    "gridY": 25,
    "height": 100,
    "layers": [
        {
            "id": "c33fce13-3623-4ad7-ab3e-2bd4595b0064",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca72f4ac-777b-42bb-98c0-530318028f6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}