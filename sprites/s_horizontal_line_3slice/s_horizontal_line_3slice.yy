{
    "id": "43d7678a-4cd2-4746-b822-550fc1f0eac0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_horizontal_line_3slice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb434eef-b103-4693-9a04-54bfcddc5024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d7678a-4cd2-4746-b822-550fc1f0eac0",
            "compositeImage": {
                "id": "265dc542-fae9-49a2-a8f4-2ba637b57f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb434eef-b103-4693-9a04-54bfcddc5024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b7c2a9-7519-447e-adfa-0d1b1a47a2b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb434eef-b103-4693-9a04-54bfcddc5024",
                    "LayerId": "ffc46751-0b01-4db1-acc1-560e75e5d300"
                }
            ]
        }
    ],
    "gridX": 7,
    "gridY": 7,
    "height": 7,
    "layers": [
        {
            "id": "ffc46751-0b01-4db1-acc1-560e75e5d300",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43d7678a-4cd2-4746-b822-550fc1f0eac0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 0,
    "yorig": 0
}