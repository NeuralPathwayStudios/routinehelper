{
    "id": "1b742681-aaa7-4e21-8750-565f9333da77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mockup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1919,
    "bbox_left": 0,
    "bbox_right": 1079,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9665e4e2-14cf-49e5-b5bd-51963b426a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b742681-aaa7-4e21-8750-565f9333da77",
            "compositeImage": {
                "id": "f1099b78-543e-4350-bbc9-c0df1e0e74e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9665e4e2-14cf-49e5-b5bd-51963b426a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa656ddd-6570-42ac-81d3-b495c83f41e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9665e4e2-14cf-49e5-b5bd-51963b426a72",
                    "LayerId": "fd4b2fce-9805-445e-89e4-cd53bdf010a3"
                },
                {
                    "id": "cf36e149-1b40-4ff6-9227-b3bd3376740c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9665e4e2-14cf-49e5-b5bd-51963b426a72",
                    "LayerId": "f14fb3c9-ba0b-40fe-9ee9-8627e023db22"
                },
                {
                    "id": "d5fd6e49-e742-4c23-b8e7-bb8fcbe5ce06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9665e4e2-14cf-49e5-b5bd-51963b426a72",
                    "LayerId": "d762b74f-2356-4311-b298-74c832ce049f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1920,
    "layers": [
        {
            "id": "d762b74f-2356-4311-b298-74c832ce049f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b742681-aaa7-4e21-8750-565f9333da77",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f14fb3c9-ba0b-40fe-9ee9-8627e023db22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b742681-aaa7-4e21-8750-565f9333da77",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fd4b2fce-9805-445e-89e4-cd53bdf010a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b742681-aaa7-4e21-8750-565f9333da77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1080,
    "xorig": 0,
    "yorig": 0
}