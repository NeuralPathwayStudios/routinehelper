{
    "id": "1f991211-7455-46d7-9ab6-3ffd8fbc55be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10cfe584-9aa6-4784-bfe9-cd2b8436643f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f991211-7455-46d7-9ab6-3ffd8fbc55be",
            "compositeImage": {
                "id": "9343bae6-243f-4b33-b244-133ea4bbcd00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10cfe584-9aa6-4784-bfe9-cd2b8436643f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b85726f-af52-42c0-bd32-7b5009811dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10cfe584-9aa6-4784-bfe9-cd2b8436643f",
                    "LayerId": "aea679d1-b594-4314-beb1-060ec8658385"
                }
            ]
        }
    ],
    "gridX": 50,
    "gridY": 50,
    "height": 100,
    "layers": [
        {
            "id": "aea679d1-b594-4314-beb1-060ec8658385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f991211-7455-46d7-9ab6-3ffd8fbc55be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}