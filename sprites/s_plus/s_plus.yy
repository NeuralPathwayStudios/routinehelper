{
    "id": "ff4a7525-8104-47c6-a4a5-5d59e8f54576",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bedf8f3-49f5-48f4-a76d-0c8f9c8e33e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff4a7525-8104-47c6-a4a5-5d59e8f54576",
            "compositeImage": {
                "id": "48d47d8f-2d79-4245-9030-d520c63822fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bedf8f3-49f5-48f4-a76d-0c8f9c8e33e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77abbeb3-eccd-4a14-ac52-ce6413d36ec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bedf8f3-49f5-48f4-a76d-0c8f9c8e33e2",
                    "LayerId": "1bab83e1-2bc6-4976-9778-a25482e822b0"
                }
            ]
        }
    ],
    "gridX": 50,
    "gridY": 50,
    "height": 100,
    "layers": [
        {
            "id": "1bab83e1-2bc6-4976-9778-a25482e822b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff4a7525-8104-47c6-a4a5-5d59e8f54576",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}