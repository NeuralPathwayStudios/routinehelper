{
    "id": "2d85b9b2-1df2-43d6-b2c5-0ac1b6c8e6ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sort",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "000f3601-7490-46d6-aaad-ecd547f6e2f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d85b9b2-1df2-43d6-b2c5-0ac1b6c8e6ef",
            "compositeImage": {
                "id": "4055d58a-9119-4a76-b08f-ccad8fe4ad85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "000f3601-7490-46d6-aaad-ecd547f6e2f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fcae32d-07b5-473d-a53b-cc405f8dfda9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "000f3601-7490-46d6-aaad-ecd547f6e2f5",
                    "LayerId": "d3411957-1dca-44a2-9b20-8fc4dd5d5a6c"
                }
            ]
        }
    ],
    "gridX": 50,
    "gridY": 50,
    "height": 100,
    "layers": [
        {
            "id": "d3411957-1dca-44a2-9b20-8fc4dd5d5a6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d85b9b2-1df2-43d6-b2c5-0ac1b6c8e6ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}