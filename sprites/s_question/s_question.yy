{
    "id": "8a9690a1-f5d1-48c2-a240-3c564fd05c53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_question",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d02324c-ab99-4432-8045-ba20ab52dbc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a9690a1-f5d1-48c2-a240-3c564fd05c53",
            "compositeImage": {
                "id": "e00acff0-a517-4711-8753-6af758d94f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d02324c-ab99-4432-8045-ba20ab52dbc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4082bcbe-6286-424b-a8b1-81c9263fddd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d02324c-ab99-4432-8045-ba20ab52dbc5",
                    "LayerId": "73a9236d-0098-4e48-8071-2eb19ca49e7b"
                }
            ]
        }
    ],
    "gridX": 50,
    "gridY": 50,
    "height": 100,
    "layers": [
        {
            "id": "73a9236d-0098-4e48-8071-2eb19ca49e7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a9690a1-f5d1-48c2-a240-3c564fd05c53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}