{
    "id": "688d2f54-6bf9-4b86-973c-3c7a317d820b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_vertical_line_3slice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6e546e3-3f47-4434-9fba-f5611eccee03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "688d2f54-6bf9-4b86-973c-3c7a317d820b",
            "compositeImage": {
                "id": "0d6acd41-d323-482d-9083-0649f2e161e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6e546e3-3f47-4434-9fba-f5611eccee03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3427c39a-69b5-4357-943c-6ba86b6b3d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6e546e3-3f47-4434-9fba-f5611eccee03",
                    "LayerId": "6e282658-adb4-4589-ba25-29344eed199e"
                }
            ]
        }
    ],
    "gridX": 7,
    "gridY": 7,
    "height": 21,
    "layers": [
        {
            "id": "6e282658-adb4-4589-ba25-29344eed199e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "688d2f54-6bf9-4b86-973c-3c7a317d820b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}